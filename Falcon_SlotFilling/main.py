#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 09:56:55 2018

@author: vaibhavi
"""


from flask import Flask, request, jsonify, Response, render_template
#from flask.ext.uploads import UploadSet, configure_uploads, DATA
#from flask_uploads import UploadSet, configure_uploads, DATA
from flask_restplus import Api, Resource, fields, reqparse
from flask_cors import CORS
import logging as log
import sys
import json
#from yaml_api_v5_one import main_yamlGeneration
from yaml_api_v6 import main_yamlGeneration


# Flask App
app = Flask(__name__)
api = Api(app, version='1.0', title='SlotFilling Yaml Generation',
          description='')
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# Logging
root = log.getLogger()
root.setLevel(log.DEBUG)
ch = log.StreamHandler(sys.stdout)
ch.setLevel(log.DEBUG)
formatter = log.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

@api.route('/slot_filling_yaml')
@api.response(200, 'Yaml Generation Successful')
@api.response(404, 'Error while generating yaml')
class Generate_SlotFilling_Yaml(Resource):
#    @api.expect(swagger_arg)
    def post(self):
        log.info('Starting request %s', request)
        body_dict = request.get_json(silent=True,force=True)  
        log.info(body_dict)
        yaml_content = main_yamlGeneration(body_dict)
        return Response(yaml_content, mimetype='text/html')

        
@app.after_request
def add_cors_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Content-Length,Cache-Control,X-Requested-With,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Cache-Control,X-Requested-With,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
    return response

if __name__ == '__main__':
    app.run()
