#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 11:35:28 2018

@author: vaibhavi
"""


#### replace source_link_set with tag related field in yaml

import json
import traceback

   
def process_TagJson(tagJson):
#    print(tagJson)
    taglist_high = [] # list to hold tags and their values - with high priority
    taglist_medium = [] #list to hold tags and their values - with medium priority
    taglist_low = [] # list to hold tags and their values - with low priority  
    try:
        for entry in tagJson:
#            print(entry)
#            print("_________")
#            valid_slot = 0
            for element in entry["tags"]:
#                print(entry["tags"])
#                if element.startswith("slot_"):
#                    valid_slot = 1
#            if valid_slot == 1:
                for key,value in entry["data"].items():
                    if key == 'priority':
                        if value == 'high':
#                            entry["data"]["name"] = entry["tags"][0]
#                            entry["data"]["slot_samples"] = entry["tags"]
#                            taglist_high.append(entry["data"])
                            taglist_high.append(entry)
                        if value == 'medium':
#                            entry["data"]["slot_samples"] = entry["tags"]
#                            entry["data"]["name"] = entry["tags"][0]
#                            taglist_medium.append(entry["data"])
                            taglist_medium.append(entry)
                        if value == 'low':
#                            entry["data"]["slot_samples"] = entry["tags"]
#                            entry["data"]["name"] = entry["tags"][0]
#                            taglist_low.append(entry["data"])
                            taglist_low.append(entry)
#        print(taglist_high)
    except Exception as je:
        print("Error in Processing Json from UI: " + str(je))
        print(traceback.format_exc())
    return taglist_high, taglist_medium, taglist_low      



def preprocess_Json(TagList):
    tag_Info = [] # details of tags 
    for listEntry in TagList:
        tag = dict()
        for key,value in listEntry.items():
            if key == "tags":
                tag['name'] = value[0]
            if key == "data":
                for key1, value1 in value.items():
                    if key1 == "name":
                        tag['attribute'] = value1
                    if key1 == "slot_questions":
                        # if more than one question, separate them with pipes and store
                        if len(value1) == 1:
                            tag['question'] = value1[0]
                        elif len(value1) > 1:
                            qvalue = ""
                            for element in value1:
                                qvalue = qvalue+element+" | "
                            tag['question'] = qvalue[:-3]
                        else:
                            tag['question'] = "none"
                    if key1 == "priority":
                        tag["priority"] = value1
                    # process options/parameters of tags
                    if key1 == "type":
                        for typekey, typeval in value1.items():
                            if typekey == 'type':
                                tag['type'] = typeval
                            if typekey == 'enum':
                                tag['parameters'] = typeval
                            else:
                                tag['parameters'] = []
        tag_Info.append(tag)
    return tag_Info


def generateYaml(tag_high,tag_medium,tag_low,context):
#    print(context)
    tag_info = []
    tag_info = tag_high+tag_medium+tag_low

    yaml_content = ""
    
    ''' declaring yaml details '''
    #### yaml details
    yaml_content = yaml_content+'---\n'
    nameProcessed = ""
    for eachname in context["name"].split():
        nameProcessed = nameProcessed+eachname+"_"
#    print(nameProcessed[:-1])

    yaml_content = yaml_content+'name: "'+nameProcessed[:-1]+'"\n'
    yaml_content = yaml_content+'name_pronounce: "'+context["name"]+'"\n'
    yaml_content = yaml_content+'version_int: 2\n'
    yaml_content = yaml_content+'yaml_version: "1.0"\n\n'
    
    
    ''' declaring yaml constants '''
    #### yaml constants

    yaml_content = yaml_content+'YAMLConstants:\n'
    yaml_content = yaml_content+'  name: "'+context["name"] +'"\n'
    yaml_content = yaml_content+'  welcome: "' + context["welcome"]+ '"\n'
    yaml_content = yaml_content+'  welcomeReturning: "'+ context["welcome_returning"] +'"\n'
    yaml_content = yaml_content+'  example: "'+ context["example"] +'"\n'
    yaml_content = yaml_content+'  nextQuestion: "'+context["next_question"]+'"\n'
    yaml_content = yaml_content+'  helpMsg: "'+ context["help_msg"]+'"\n'
    yaml_content = yaml_content+'  yesNoHelpMsg: "'+context["yes_no_help_msg"]+'"\n'
    yaml_content = yaml_content+'  restApiUri: "'+context["rest_api_base_url"] +'/api"\n'
    yaml_content = yaml_content+'  bye: "'+context["bye"]+'"\n'
    yaml_content = yaml_content+'  error: "'+context["error"]+'"\n'
    yaml_content = yaml_content+'  offerLongAnswerQA: "'+context["offer_long_answer_qa"]+'"\n'
    yaml_content = yaml_content+'  offerLongAnswerES: "'+context["offer_long_answer_es"] +'"\n'
    yaml_content = yaml_content+'  noAnswer: "'+context["no_answer"]+ '"\n'
    yaml_content = yaml_content+'  noLong: "'+context["no_long"] +'"\n'
    yaml_content = yaml_content+'  noLongButEmail: "'+context["no_long_but_email"]+'"\n'
    yaml_content = yaml_content+'  emailSent: "'+context["email_sent"]+'"\n'
   
    
    ''' declaring turnCounter related values'''
    #### turn counter related lines in yaml
    yaml_content = yaml_content+'\n  turnCounter: "0"\n  turnOne: "1"\n  turnTwo: "2"\n  turnThree: "3"\n  turnInit: "0"\n  turnTransaction: "false"\n\n'
    ''' default recommendation msg '''
    yaml_content = yaml_content+'  default_rcmd: "Ask me another question."\n' 
    ''' rcmd from current question stored for rcmd handling in fillslot node'''
    yaml_content = yaml_content+'  rcmd_value: "null"\n'
    ''' dummy value for button functionality handling '''
    yaml_content = yaml_content+'  dummyValue: "null"\n\n\n'
    ''' declaring tags '''
    ### declaring tags and related info in yaml
    ### tag is referered as 'field' for avoiding confusion
    ### slot is used for holding value
    for i,tagEntry in enumerate(tag_info, start = 1):
        ### setting field to tagname
        if i < 10:
            yaml_content = yaml_content+'  field0'+str(i)+': "'+tagEntry['name']+'"\n'
            ### setting attribute to tag attribute
            yaml_content = yaml_content+'  field0'+str(i)+'_attribute: "'+tagEntry['attribute']+'"\n'
            ### setting field_priority to tag priority
            yaml_content = yaml_content+'  field0'+str(i)+'_priority: "'+tagEntry['priority']+'"\n'
#            print(yaml_content)
            ### setting field_type to field type
            yaml_content = yaml_content+'  field0'+str(i)+'_type: "'+tagEntry['type']+'"\n'
            ### setting field_question to tag question
            yaml_content = yaml_content+'  field0'+str(i)+'_question: "'+tagEntry['question']+'"\n'
            ### setting field_parameters to tag parameters only for fixed fields
            ### for free fileds no parameter will be generated
#            if len(tagEntry['parameters']) > 0:
#                for j,param in enumerate(tagEntry['parameters'], start = 1):
#                    if j < 10:
#                        f.write('  field0'+str(i)+'_param0'+str(j)+': "'+param+'"\n')
#                    else: 
#                        f.write('  field0'+str(i)+'_param'+str(j)+': "'+param+'"\n')
            yaml_content = yaml_content+'  slot0'+str(i)+'_transaction: "false"\n'
            yaml_content = yaml_content+'  slot0'+str(i)+': "null"\n\n'
        else:
            yaml_content = yaml_content+'  field'+str(i)+': "'+tagEntry['name']+'"\n'
            ### setting attribute to tag attribute
            yaml_content = yaml_content+'  field'+str(i)+'_attribute: "'+tagEntry['attribute']+'"\n'
            ### setting field_priority to tag priority
            yaml_content = yaml_content+'  field'+str(i)+'_priority: "'+tagEntry['priority']+'"\n'
            ### setting field_type to tag type
            yaml_content = yaml_content+'  field'+str(i)+'_type: "'+tagEntry['type']+'"\n'
            ### setting field_question to tag question
            yaml_content = yaml_content+'  field'+str(i)+'_question: "'+tagEntry['question']+'"\n'
            ### setting field_parameters to tag parameters only for fixed fields
            ### for free fileds no parameter will be generated
#            if len(tagEntry['parameters']) > 0:
#                for j,param in enumerate(tagEntry['parameters'], start = 1):
#                    if j < 10:
#                        f.write('  field'+str(i)+'_param0'+str(j)+': "'+param+'"\n')
#                    else: 
#                        f.write('  field'+str(i)+'_param'+str(j)+': "'+param+'"\n')
            yaml_content = yaml_content+'  slot'+str(i)+'_transaction: "false"\n'
            yaml_content = yaml_content+'  slot'+str(i)+': "null"\n\n'
            
    ''' declaring templateId, methodId, confidenceThreshold'''
    ### templateId, methodId, confidenceThreshold    
    yaml_content = yaml_content+'  templateId: "'+context["template_id"]+'"\n'
    yaml_content = yaml_content+'  methodId: "'+context["method_id"]+'"\n'
    yaml_content = yaml_content+'  confidenceThreshold: "'+context["confidence_threshold"]+'"\n\n'
    ### help msg
#    yaml_content = yaml_content+'help: "(${dialog.CONST.helpMsg}) <break time=\\"250ms\\"/> (${dialog.CONST.nextQuestion})"\n\n'
    yaml_content = yaml_content+'help: "(${dialog.CONST.helpMsg})"\n\n'
#    print(yaml_content)
    
    ''' NodeList begins here '''
    #### Nodelist declaration begins 
    yaml_content = yaml_content+'NodeList:\n'
    
    ''' welcomeNode '''
    #### welcomeNode actions
    # all actions are static for this node
    yaml_content = yaml_content+'  welcomeNode:\n'
    yaml_content = yaml_content+'    node_type: "WELCOMEPOINT"\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "SAY"\n'
    yaml_content = yaml_content+'      condition: \"!avail(\'${global.current.returning_user}\')\"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.welcome}"\n'
    yaml_content = yaml_content+'    - action_02: "SAY"\n'
    yaml_content = yaml_content+'      condition: \"avail(\'${global.current.returning_user}\')\"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.welcomeReturning} "\n'
    yaml_content = yaml_content+'    - action_03: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${global.current.returning_user} TO true"\n'
    yaml_content = yaml_content+'    - action_04: "GOTO"\n'
    yaml_content = yaml_content+'      get_user_response: true\n'
    yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'
    
    ''' getAnswer '''
    #### getAnswer - Entrypoint node
    # 3 actions at the start - static
    # 3 actions at the end - static
    # len(tagEntry) gives the no . of tags for which actions will be repeated 
    getAnswer_actions = 3 + len(tag_info) + 3
    action_flag = 0
    for actionNumber,val in enumerate(list(range(1,getAnswer_actions+1)), start = 1):
        if actionNumber == 1:
            # getAnswer node details
            yaml_content = yaml_content+'  getAnswer:\n'
            yaml_content = yaml_content+'    node_type: "ENTRYPOINT"\n'
            yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
            # action_01 - static - API call details
            yaml_content = yaml_content+'    - action_01: "CALL_REST_API"\n'
            yaml_content = yaml_content+'      rest_01_name: "ENDPOINT_INFO"\n'
            yaml_content = yaml_content+'      rest_01_param: \"{\'url\':\'${dialog.CONST.restApiUri}\', \'method\': \'GET\', \'endpoint\': \'/batch/\',\'param_name\': \'params\' }\"\n'
            yaml_content = yaml_content+'      rest_02_name: "DATA_IN"\n'
            yaml_content = yaml_content+'      rest_02_param: \"{\'method_id\': \'alexa\', \'text\': \'${dialog.userInputLast}\'}\"\n'
            yaml_content = yaml_content+'      rest_03_name: "INTENT"\n'
            yaml_content = yaml_content+'      rest_03_param: \"{\'route\': \'qa\', \'method_id\': \'${dialog.CONST.methodId}\', \'template_id\': \'${dialog.CONST.templateId}\', \'output_type\': [\'short\', \'long\'], \'save_to\': \'qa_response\' }"\n'
        if actionNumber == 2: 
            yaml_content = yaml_content+'    - action_02: "SAY"\n'
            yaml_content = yaml_content+'      condition: \"!avail(\'${dialog.qa_response}\') || !avail(\'${dialog.qa_response.api_call_status}\') || !${dialog.qa_response.api_call_status.ok}\"\n'
            yaml_content = yaml_content+'      reply: "${dialog.CONST.error}"\n'
        if actionNumber == 3:
            yaml_content = yaml_content+'    - action_03: "GOTO"\n'
            yaml_content = yaml_content+'      goto_node: \"exitNode IF !avail(\'${dialog.qa_response}\') || !avail(\'${dialog.qa_response.api_call_status}\') || !${dialog.qa_response.api_call_status.ok}\"\n'
        if actionNumber == 4:
            for slotAction,tagEntry in enumerate(tag_info, start = 1):
                if actionNumber < 10:
                    if slotAction < 10:
                        yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot0'+str(slotAction)+' IF ((avail(\'${dialog.CONST.slot0'+str(slotAction)+'_transaction}\') && ${dialog.CONST.slot0'+str(slotAction)+'_transaction} == \'true\') && (avail(\'${dialog.CONST.slot0'+str(slotAction)+'}\') && ${dialog.CONST.slot0'+str(slotAction)+'} == \'null\'))\"\n'
                        actionNumber = actionNumber + 1
                    else:
                        yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot'+str(slotAction)+' IF ((avail(\'${dialog.CONST.slot'+str(slotAction)+'_transaction}\') && ${dialog.CONST.slot'+str(slotAction)+'_transaction} == \'true\') && (avail(\'${dialog.CONST.slot'+str(slotAction)+'}\') && ${dialog.CONST.slot'+str(slotAction)+'} == \'null\'))\"\n'
                        actionNumber = actionNumber + 1
                else:
                    if slotAction < 10:
                        yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot0'+str(slotAction)+' IF ((avail(\'${dialog.CONST.slot0'+str(slotAction)+'_transaction}\') && ${dialog.CONST.slot0'+str(slotAction)+'_transaction} == \'true\') && (avail(\'${dialog.CONST.slot0'+str(slotAction)+'}\') && ${dialog.CONST.slot0'+str(slotAction)+'} == \'null\'))\"\n'
                        actionNumber = actionNumber + 1
                    else:
                        yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot'+str(slotAction)+' IF ((avail(\'${dialog.CONST.slot'+str(slotAction)+'_transaction}\') && ${dialog.CONST.slot'+str(slotAction)+'_transaction} == \'true\') && (avail(\'${dialog.CONST.slot'+str(slotAction)+'}\') && ${dialog.CONST.slot'+str(slotAction)+'} == \'null\'))\"\n'
                        actionNumber = actionNumber + 1
        if actionNumber == (3 + len(tag_info) + 1):
            if action_flag == 0:
                if actionNumber < 10:
                    yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                    yaml_content = yaml_content+'      goto_node: \"haveAnswer IF avail(\'${dialog.qa_response.api_qa.final_res}\') && ( ${dialog.qa_response.api_qa.final_res_conf} > ${dialog.CONST.confidenceThreshold} )"\n'
                    action_flag = 1
                else:
                    yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                    yaml_content = yaml_content+'      goto_node: \"haveAnswer IF avail(\'${dialog.qa_response.api_qa.final_res}\') && ( ${dialog.qa_response.api_qa.final_res_conf} > ${dialog.CONST.confidenceThreshold} )"\n'
                    action_flag = 1
        if actionNumber == (3 + len(tag_info) + 2):
            if actionNumber < 10:
                yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "SAY"\n'
                yaml_content = yaml_content+'      reply: "${dialog.CONST.noAnswer} <break time=\\"250ms\\"/> (${dialog.CONST.nextQuestion})"\n'
            else:
                yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "SAY"\n'
                yaml_content = yaml_content+'      reply: "${dialog.CONST.noAnswer} <break time=\\"250ms\\"/> (${dialog.CONST.nextQuestion})"\n'
        if actionNumber == (3 + len(tag_info) + 3):
            if actionNumber < 10:
                yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
                yaml_content = yaml_content+'      get_user_response: true\n\n'
            else:
                yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
                yaml_content = yaml_content+'      get_user_response: true\n\n'
    
    ''' haveAnswer '''
    #### haveAnswer Node
    # all actions are static for this node
    yaml_content = yaml_content+'  haveAnswer:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "UNSET"\n'
    yaml_content = yaml_content+'      unset_variable: "${dialog.botRepliesRcmd}"\n'
    yaml_content = yaml_content+'    - action_02: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.rcmd_value} TO ${dialog.qa_response.api_qa.final_rcmd_short} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
    # says default rcmd if question does not have a rcmd
    yaml_content = yaml_content+'    - action_03: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.rcmd_value} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} == \'N/A\' || ${dialog.qa_response.api_qa.final_rcmd_short} == \'\')\"\n'
    
    yaml_content = yaml_content+'    - action_04: "SAY"\n'
    yaml_content = yaml_content+'      reply: "${dialog.qa_response.api_qa.final_res}"\n'
    yaml_content = yaml_content+'    - action_05: "GOTO"\n'
#    yaml_content = yaml_content+'      goto_node: \"checkFieldWithTags IF (avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags} != \'\')"\n'
    yaml_content = yaml_content+'      goto_node: \"checkFieldWithTags IF (avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags} != \'N/A\' && ${dialog.qa_response.api_qa.tags} != \'\')"\n'
    yaml_content = yaml_content+'    - action_06: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: \"turnCountNode"\n'
#    f.write('      goto_node: \"turnCountNode IF (avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags} == \'\')"\n')
    yaml_content = yaml_content+'    - action_07: "HELP_FOR_NEXT_NODE"\n'
    yaml_content = yaml_content+'      help: "${dialog.CONST.helpMsg}"\n'
    yaml_content = yaml_content+'    - action_08: "GOTO"\n'
    yaml_content = yaml_content+'      get_user_response: true\n'
    yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'

    ''' turnCountNode ''' 
    #### this node increases the turnCounter value by 1 under these 2 cases:
    # when a question without a tag is uttered by end-user
    # when a question with a tag is uttered by end-user but the tag value is already filled
    #### turnCountNode
    yaml_content = yaml_content+'  turnCountNode:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.turnCounter} TO ${dialog.CONST.turnThree} IF (avail(\'${dialog.CONST.turnCounter}\') && ${dialog.CONST.turnCounter} == \'2\')\"\n'
    yaml_content = yaml_content+'    - action_02: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.turnCounter} TO ${dialog.CONST.turnTwo} IF (avail(\'${dialog.CONST.turnCounter}\') && ${dialog.CONST.turnCounter} == \'1\')\"\n'
    yaml_content = yaml_content+'    - action_03: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.turnCounter} TO ${dialog.CONST.turnOne} IF (avail(\'${dialog.CONST.turnCounter}\') && ${dialog.CONST.turnCounter} == \'0\')\"\n'
    yaml_content = yaml_content+'    - action_04: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: \"fillFormNode IF (avail(\'${dialog.CONST.turnCounter}\') && ${dialog.CONST.turnCounter} == \'3\')\"\n'
#    yaml_content = yaml_content+'    - action_05: "SAY"\n'
#    yaml_content = yaml_content+'      condition: \"(avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
#    yaml_content = yaml_content+'      reply: "${dialog.qa_response.api_qa.final_rcmd_short}"\n'
    # says rcmd of the question, if available
    yaml_content = yaml_content+'    - action_05: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.qa_response.api_qa.final_rcmd_short} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
    # says default rcmd if question does not have a rcmd
    yaml_content = yaml_content+'    - action_06: "SET"\n'
    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} == \'N/A\' || ${dialog.qa_response.api_qa.final_rcmd_short} == \'\')\"\n'
    yaml_content = yaml_content+'    - action_07: "GOTO"\n'
    yaml_content = yaml_content+'      get_user_response: true\n'
    yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'

    ''' fillFormNode '''
    #### this node helps in invoking the fillSlotn node
    # the fillSlotn nodes are arranged in actions according to priority
    # all slots with high priority are listed first followed by medium and finally by low
    # if slot is already filled, the fillSlotn of that slot will not be invoked
    # 1 action at the start - static
    # 3 actions at the end - static
    # len(tagEntry) gives the no . of tags for which actions will be repeated 
    fillForm_actions = 1 + len(tag_info) + 3
    fill_action_flag = 0
    for actionNumber,val in enumerate(list(range(1,fillForm_actions+1)), start = 1):
        if actionNumber == 1:
            # fillFormNode node details
            yaml_content = yaml_content+'  fillFormNode:\n'
            yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
            # action_01
            yaml_content = yaml_content+'    - action_01: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.turnTransaction} TO true"\n'
        if actionNumber == 2:
            for fill_slotAction,tagEntry in enumerate(tag_info, start = 1):
                if actionNumber < 10:
                    if fill_slotAction < 10:
                        yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: \"fillSlot0'+str(fill_slotAction)+' IF (avail(\'${dialog.CONST.slot0'+str(fill_slotAction)+'}\') && ${dialog.CONST.slot0'+str(fill_slotAction)+'} == \'null\')"\n'
                        actionNumber = actionNumber + 1
                    else:
                        yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: \"fillSlot'+str(fill_slotAction)+' IF (avail(\'${dialog.CONST.slot'+str(fill_slotAction)+'}\') && ${dialog.CONST.slot'+str(fill_slotAction)+'} == \'null\')"\n'
                        actionNumber = actionNumber + 1
                else:
                    if fill_slotAction < 10:
                        yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: \"fillSlot0'+str(fill_slotAction)+' IF (avail(\'${dialog.CONST.slot0'+str(fill_slotAction)+'}\') && ${dialog.CONST.slot0'+str(fill_slotAction)+'} == \'null\')"\n'
                        actionNumber = actionNumber + 1
                    else:
                        yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: \"fillSlot'+str(fill_slotAction)+' IF (avail(\'${dialog.CONST.slot'+str(fill_slotAction)+'}\') && ${dialog.CONST.slot'+str(fill_slotAction)+'} == \'null\')"\n'
                        actionNumber = actionNumber + 1
                    
        if actionNumber == (1 + len(tag_info) + 1):
            if fill_action_flag == 0:
                if actionNumber < 10:
                    yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "SET"\n'
                    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.qa_response.api_qa.final_rcmd_short} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
                    fill_action_flag = 1
                else:                    
                    yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "SET"\n'
                    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.qa_response.api_qa.final_rcmd_short} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
                    fill_action_flag = 1
                    
                    
        if actionNumber == (1 + len(tag_info) + 2):
            if actionNumber < 10:
                yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} == \'N/A\' || ${dialog.qa_response.api_qa.final_rcmd_short} == \'\')\"\n'
            else:
                yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} == \'N/A\' || ${dialog.qa_response.api_qa.final_rcmd_short} == \'\')\"\n'
        
                

        if actionNumber == (1 + len(tag_info) + 3):
            if actionNumber < 10:
                yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
                yaml_content = yaml_content+'      get_user_response: true\n\n'
            else:
                yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
                yaml_content = yaml_content+'      get_user_response: true\n\n'
        
    
    ''' checkFieldWithTags '''
    ### this node checks the tagname from the question/user's utterance with fieldname in yaml
    # if a match is found: it then checks if the slot is null or filled
    # if slot is null, it invokes that fillSlotn node respectively
    # 0 action at the start - static
    # 1 actions at the end - static
    # len(tagEntry) gives the no . of tags for which actions will be repeated 
    checkField_actions = len(tag_info) + len(tag_info) + 1
    check_action_flag1 = 0 # to avoid generating the step twice after loop of tag fields
    check_action_flag2 = 0 # to avoid generating the step twice after loop of tag fields
    # checkFieldWithTags node details
    yaml_content = yaml_content+'  checkFieldWithTags:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    for actionNumber,val in enumerate(list(range(1,checkField_actions+1)), start = 1):
        if actionNumber == 1:
            for check_slotAction,tagEntry in enumerate(tag_info, start = 1):
                if actionNumber < 10:
                    if check_slotAction < 10:
                        yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot0'+str(check_slotAction)+' IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot0'+str(check_slotAction)+'}\') && ${dialog.CONST.slot0'+str(check_slotAction)+'} == \'null\'))"\n'
                        actionNumber = actionNumber + 1
                    else:
                        yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot'+str(check_slotAction)+' IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot'+str(check_slotAction)+'}\') && ${dialog.CONST.slot'+str(check_slotAction)+'} == \'null\'))"\n'
                        actionNumber = actionNumber + 1
                else:
                    if check_slotAction < 10:
                        yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot0'+str(check_slotAction)+' IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot0'+str(check_slotAction)+'}\') && ${dialog.CONST.slot0'+str(check_slotAction)+'} == \'null\'))"\n'
                        actionNumber = actionNumber + 1
                    else:
                        yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                        yaml_content = yaml_content+'      goto_node: "fillSlot'+str(check_slotAction)+' IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot'+str(check_slotAction)+'}\') && ${dialog.CONST.slot'+str(check_slotAction)+'} == \'null\'))"\n'
                        actionNumber = actionNumber + 1
                    
        if actionNumber == (len(tag_info) + 1):
            if check_action_flag1 == 0:
                for check_slotAction,tagEntry in enumerate(tag_info, start = 1):
                    if actionNumber < 10:
                        if check_slotAction < 10:
                            yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                            yaml_content = yaml_content+'      goto_node: "turnCountNode IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot0'+str(check_slotAction)+'}\') && ${dialog.CONST.slot0'+str(check_slotAction)+'} != \'null\'))"\n'
                            actionNumber = actionNumber + 1
                        else:
                            yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                            yaml_content = yaml_content+'      goto_node: "turnCountNode IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot'+str(check_slotAction)+'}\') && ${dialog.CONST.slot'+str(check_slotAction)+'} != \'null\'))"\n'
                            actionNumber = actionNumber + 1
                    else:
                        if check_slotAction < 10:
                            yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                            yaml_content = yaml_content+'      goto_node: "turnCountNode IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot0'+str(check_slotAction)+'}\') && ${dialog.CONST.slot0'+str(check_slotAction)+'} != \'null\'))"\n'
                            actionNumber = actionNumber + 1
                        else:
                            yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                            yaml_content = yaml_content+'      goto_node: "turnCountNode IF ((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(check_slotAction)+'}) && (avail(\'${dialog.CONST.slot'+str(check_slotAction)+'}\') && ${dialog.CONST.slot'+str(check_slotAction)+'} != \'null\'))"\n'
                            actionNumber = actionNumber + 1
                check_action_flag1 = 1
                
        if actionNumber == (len(tag_info) + len(tag_info) + 1):
            if check_action_flag2 == 0:
                if actionNumber < 10:
                    yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "SET"\n'
                    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.qa_response.api_qa.final_rcmd_short} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
                    actionNumber = actionNumber + 1
                    # says default rcmd if question does not have a rcmd
                    yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "SET"\n'
                    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} == \'N/A\' || ${dialog.qa_response.api_qa.final_rcmd_short} == \'\')\"\n'
                    actionNumber = actionNumber + 1
                    yaml_content = yaml_content+'    - action_0'+str(actionNumber)+': "GOTO"\n'
                    yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
                    yaml_content = yaml_content+'      get_user_response: true\n\n'
                else:
                    yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "SET"\n'
                    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.qa_response.api_qa.final_rcmd_short} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} != \'N/A\' &&  ${dialog.qa_response.api_qa.final_rcmd_short} != \'\')\"\n'
                    actionNumber = actionNumber + 1
                    # says default rcmd if question does not have a rcmd
                    yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "SET"\n'
                    yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.qa_response.api_qa.final_rcmd_short}\') && ${dialog.qa_response.api_qa.final_rcmd_short} == \'N/A\' || ${dialog.qa_response.api_qa.final_rcmd_short} == \'\')\"\n'
                    actionNumber = actionNumber + 1
                    yaml_content = yaml_content+'    - action_'+str(actionNumber)+': "GOTO"\n'
                    yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
                    yaml_content = yaml_content+'      get_user_response: true\n\n'
                check_action_flag2 = 1
                
    ''' fillSlotn Node'''
    ### fillSlot nodes will be generated n times
    # n is the no of tags in the tagEntry list
    # tags are already sorted according to priority and placed in tagEntry list
    # the order in tagEntry is used to generate the fillSlotn nodes
    
    for fieldnumber, tagEntry in enumerate(tag_info, start = 1):
        if fieldnumber < 10:
            yaml_content = yaml_content+'  fillSlot0'+str(fieldnumber)+':\n'
            yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
            yaml_content = yaml_content+'    - action_01: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.slot0'+str(fieldnumber)+'} TO ${dialog.userInputLast} IF ((avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'_transaction}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'_transaction} == \'true\') && (avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\'))\"\n'
            yaml_content = yaml_content+'    - action_02: "SAY"\n'
            yaml_content = yaml_content+'      condition: (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\'))\n'
            yaml_content = yaml_content+'      reply: "${dialog.CONST.field0'+str(fieldnumber)+'_question}"\n'
            yaml_content = yaml_content+'    - action_03: "SET"\n'
            if len(tagEntry['parameters']) > 0:
                value = '      set_variable: \"${dialog.botRepliesRcmd} TO '
                for pnum, param in enumerate(tagEntry['parameters'], start = 1):
#                        question = question + '${dialog.CONST.field0'+str(fieldnumber)+'_param0'+str(pnum)+'} or '
                    value = value + param + '|'
                yaml_content = yaml_content+value[:-1]+' IF (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\'))"\n'
            else:
                value = '      set_variable: \"${dialog.CONST.dummyValue} TO null'
                yaml_content = yaml_content+value+' IF (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\'))"\n'
#                print(value[:-1])
                
            yaml_content = yaml_content+'    - action_04: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.slot0'+str(fieldnumber)+'_transaction} TO true IF (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field0'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\'))"\n'
            yaml_content = yaml_content+'    - action_05: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.turnCounter} TO ${dialog.CONST.turnInit} IF ((avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\'))"\n'
            yaml_content = yaml_content+'    - action_06: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.turnTransaction} TO false"\n'
            yaml_content = yaml_content+'    - action_07: "GOTO"\n'
            yaml_content = yaml_content+'      get_user_response: true\n'
            yaml_content = yaml_content+'      goto_node: "getAnswer IF (avail(\'${dialog.CONST.slot0'+str(fieldnumber)+'}\') && ${dialog.CONST.slot0'+str(fieldnumber)+'} == \'null\')\"\n'
            yaml_content = yaml_content+'    - action_08: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.slot0'+str(fieldnumber)+'_transaction} TO false"\n'
            yaml_content = yaml_content+'    - action_09: "UNSET"\n'
            yaml_content = yaml_content+'      unset_variable: "${dialog.botRepliesRcmd}"\n'
                
        else:
            yaml_content = yaml_content+'  fillSlot'+str(fieldnumber)+':\n'
            yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
            yaml_content = yaml_content+'    - action_01: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.slot'+str(fieldnumber)+'} TO ${dialog.userInputLast} IF (avail(\'${dialog.CONST.slot'+str(fieldnumber)+'_transaction}\') && ${dialog.CONST.slot'+str(fieldnumber)+'_transaction} == \'true\')"\n'
            yaml_content = yaml_content+'    - action_02: "SAY"\n'
            yaml_content = yaml_content+'      condition: (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot'+str(fieldnumber)+'}\') && ${dialog.CONST.slot'+str(fieldnumber)+'} == \'null\'))\n'
            yaml_content = yaml_content+'      reply: "${dialog.CONST.field'+str(fieldnumber)+'_question}"\n'
            yaml_content = yaml_content+'    - action_03: "SET"\n'
            if len(tagEntry['parameters']) > 0:
                value = '      set_variable: \"${dialog.botRepliesRcmd} TO '
                for pnum, param in enumerate(tagEntry['parameters'], start = 1):
#                        question = question + '${dialog.CONST.field0'+str(fieldnumber)+'_param0'+str(pnum)+'} or '
                    value = value + param + '|'
                yaml_content = yaml_content+value[:-1]+' IF (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot'+str(fieldnumber)+'}\') && ${dialog.CONST.slot'+str(fieldnumber)+'} == \'null\'))"\n'
            else:
                value = '      set_variable: \"${dialog.CONST.dummyValue} TO null'
                yaml_content = yaml_content+value+' IF (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot'+str(fieldnumber)+'}\') && ${dialog.CONST.slot'+str(fieldnumber)+'} == \'null\'))"\n'
            
            
            yaml_content = yaml_content+'    - action_04: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.slot'+str(fieldnumber)+'_transaction} TO true IF (((avail(\'${dialog.qa_response.api_qa.tags}\') && ${dialog.qa_response.api_qa.tags[0]} == ${dialog.CONST.field'+str(fieldnumber)+'}) || (avail(\'${dialog.CONST.turnTransaction}\') && ${dialog.CONST.turnTransaction} == \'true\')) && (avail(\'${dialog.CONST.slot'+str(fieldnumber)+'}\') && ${dialog.CONST.slot'+str(fieldnumber)+'} == \'null\'))"\n'
            yaml_content = yaml_content+'    - action_05: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.turnCounter} TO ${dialog.CONST.turnInit} IF ((avail(\'${dialog.CONST.slot'+str(fieldnumber)+'}\') && ${dialog.CONST.slot'+str(fieldnumber)+'} == \'null\'))"\n'
            yaml_content = yaml_content+'    - action_06: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.turnTransaction} TO false"\n'
            yaml_content = yaml_content+'    - action_07: "GOTO"\n'
            yaml_content = yaml_content+'      get_user_response: true\n'
            yaml_content = yaml_content+'      goto_node: "getAnswer IF (avail(\'${dialog.CONST.slot'+str(fieldnumber)+'}\') && ${dialog.CONST.slot'+str(fieldnumber)+'} == \'null\')\"\n'
            yaml_content = yaml_content+'    - action_08: "SET"\n'
            yaml_content = yaml_content+'      set_variable: "${dialog.CONST.slot'+str(fieldnumber)+'_transaction} TO false"\n'
            yaml_content = yaml_content+'    - action_09: "UNSET"\n'
            yaml_content = yaml_content+'      unset_variable: "${dialog.botRepliesRcmd}"\n'
            
        nameTagFound = 0
        for tagnum, tagEntry in enumerate(tag_info, start = 1):
#            print(tag_info)
#            print(tasgnum)
#            print(tasgEntry)
#            print(tagEntry['name'])
#            print(tagEsntry['question'])
#            print("----------------------")
            if (tagEntry['name']).lower() == 'name':
                nameTagFound = 1
                nameTagNum = tagnum
#                print("param found--->",nameTagNum, tagEntry['name'])
                break
        if nameTagFound == 1:
            if nameTagNum < 10:
                
                yaml_content = yaml_content+'    - action_10: "SAY"\n'
                yaml_content = yaml_content+'      condition: "((avail(\'${dialog.CONST.slot0'+str(nameTagNum)+'}\') && ${dialog.CONST.slot0'+str(nameTagNum)+'} != \'null\'))"\n'
#                yaml_content = yaml_content+'      reply: "${dialog.CONST.slot0'+str(nameTagNum)+'} please continue"\n'
                yaml_content = yaml_content+'      reply: "${dialog.CONST.slot0'+str(nameTagNum)+'}, Thanks for Sharing.|Got it, ${dialog.CONST.slot0'+str(nameTagNum)+'}.\"\n'
                yaml_content = yaml_content+'    - action_11: "SAY"\n'
                yaml_content = yaml_content+'      condition: "((avail(\'${dialog.CONST.slot0'+str(nameTagNum)+'}\') && ${dialog.CONST.slot0'+str(nameTagNum)+'} == \'null\'))"\n'
#                yaml_content = yaml_content+'      reply: "please continue"\n'
                yaml_content = yaml_content+'      reply: "Thanks for sharing.|I will keep that in mind."\n'
                
                yaml_content = yaml_content+'    - action_12: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.rcmd_value} IF (avail(\'${dialog.CONST.rcmd_value}\') && ${dialog.CONST.rcmd_value} != \'null\' &&  ${dialog.CONST.rcmd_value} != \'\')\"\n'
                yaml_content = yaml_content+'    - action_13: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.CONST.rcmd_value}\') && ${dialog.CONST.rcmd_value} == \'null\' &&  ${dialog.CONST.rcmd_value} == \'\')\"\n'
                    
                yaml_content = yaml_content+'    - action_14: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.rcmd_value} TO null"\n'
                
                yaml_content = yaml_content+'    - action_15: "GOTO"\n'
                yaml_content = yaml_content+'      get_user_response: true\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'
                
            else:
                yaml_content = yaml_content+'    - action_10: "SAY"\n'
                yaml_content = yaml_content+'      condition: "((avail(\'${dialog.CONST.slot'+str(nameTagNum)+'}\') && ${dialog.CONST.slot'+str(nameTagNum)+'} != \'null\'))"\n'
#                yaml_content = yaml_content+'      reply: "${dialog.CONST.slot'+str(nameTagNum)+'} please continue"\n'
                yaml_content = yaml_content+'      reply: "${dialog.CONST.slot'+str(nameTagNum)+'}, Thanks for Sharing.|Got it, ${dialog.CONST.slot'+str(nameTagNum)+'}\"\n'
                yaml_content = yaml_content+'    - action_11: "SAY"\n'
#                yaml_content = yaml_content+'      reply: "please continue"\n'
                yaml_content = yaml_content+'      reply: "Thanks for sharing.|I will keep that in mind."\n'
                
                yaml_content = yaml_content+'    - action_12: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.rcmd_value} IF (avail(\'${dialog.CONST.rcmd_value}\') && ${dialog.CONST.rcmd_value} != \'null\' &&  ${dialog.CONST.rcmd_value} != \'\')\"\n'
                yaml_content = yaml_content+'    - action_13: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.CONST.rcmd_value}\') && ${dialog.CONST.rcmd_value} == \'null\' &&  ${dialog.CONST.rcmd_value} == \'\')\"\n'
                    
                yaml_content = yaml_content+'    - action_14: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.rcmd_value} TO null"\n'
                
                yaml_content = yaml_content+'    - action_15: "GOTO"\n'
                yaml_content = yaml_content+'      get_user_response: true\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'
                
        else:
                yaml_content = yaml_content+'    - action_10: "SAY"\n'
#                yaml_content = yaml_content+'      reply: "please continue"\n'
                yaml_content = yaml_content+'      reply: "Thanks for sharing.|I will keep that in mind."\n'
                
                yaml_content = yaml_content+'    - action_11: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.rcmd_value} IF (avail(\'${dialog.CONST.rcmd_value}\') && ${dialog.CONST.rcmd_value} != \'null\' &&  ${dialog.CONST.rcmd_value} != \'\')\"\n'
                yaml_content = yaml_content+'    - action_12: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.botRepliesRcmd} TO ${dialog.CONST.default_rcmd} IF (avail(\'${dialog.CONST.rcmd_value}\') && ${dialog.CONST.rcmd_value} == \'null\' &&  ${dialog.CONST.rcmd_value} == \'\')\"\n'
                    
                yaml_content = yaml_content+'    - action_13: "SET"\n'
                yaml_content = yaml_content+'      set_variable: \"${dialog.CONST.rcmd_value} TO null"\n'
                
                yaml_content = yaml_content+'    - action_14: "GOTO"\n'
                yaml_content = yaml_content+'      get_user_response: true\n'
                yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'
                
                
        
        
    ''' byeNode '''
    #### byeNode actions
    # this node has all static actions
    
    yaml_content = yaml_content+'  byeNode:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "SAY"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.bye}"\n'
    yaml_content = yaml_content+'    - action_02: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "exitNode"\n\n'

    ''' exitNode '''
    #### exitNode actions
    # this node has all static actions
    
    yaml_content = yaml_content+'  exitNode:\n'
    yaml_content = yaml_content+'    node_type: "EXITPOINT"\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "END"\n'
    
#    print(yaml_content)
    
#    with open('slot_rcmd_test.yaml','w') as af:
#        af.write(yaml_content)
    
    return yaml_content
    
def main_yamlGeneration(json_filename):
#    print(json_filename["context"])
#    print(json_filename["data_set_list"])
#    tagJson = readJson(json_filename)
#    tagJson = readJson(json_filename["data_set_list"])
#    taglist_high, taglist_medium, taglist_low = process_TagJson(tagJson)
    try:
        taglist_high, taglist_medium, taglist_low = process_TagJson(json_filename["data_set_list"])
        tag_high = preprocess_Json(taglist_high)
        tag_medium = preprocess_Json(taglist_medium)
        tag_low = preprocess_Json(taglist_low)
#        generateYaml(tag_high,tag_medium,tag_low,templateID,yaml_filename)
        yaml_content = generateYaml(tag_high,tag_medium,tag_low,json_filename["context"])
        print("@@@@@@Yaml Generation Complete@@@@@@@")
        return yaml_content
    except:
        print("err..")
        return ""
            
#if __name__ == '__main__':
#    yaml_content = main_yamlGeneration(input_json)


#with open("payload_yaml.json") as f:
#    data = json.load(f)
#    print(data)
        
#main_yamlGeneration(data)