#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 13:02:01 2018

@author: vaibhavi
"""
import json

input_json = {
  "context": {
    "name": "slot test integration",
    "example": "ex",
    "welcome": "welcome user",
    "welcome_returning": "welcome again",
    "next_question": "you can ask any question",
    "help_msg": "you can ask me more questions",
    "yes_no_help_msg": "yes no node",
    "bye": "bye. See you again. ",
    "error": "Sorry there seems to be some problem",
    "offer_long_answer_qa": "There seems to be a detailed",
    "offer_long_answer_es": "There is a detailed answer too",
    "no_answer": "Sorry we dont have an answer",
    "no_long": "There is no long answer",
    "no_long_but_email": "There is no long answer but email is available",
    "email_sent": "An email has been sent",
    "rest_api_base_url": "http://34.235.43.238:3128",
    "confidence_threshold": "0.5",
    "method_id": "sent2vec_v2",
    "template_id": "5bdbed93c9e77c00084670a0"
  },
  "data_set_list": [{
		"schema": "SLOT_V1",
		"tags": ["aName"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Name",
			"slot_questions": [
				"Its nice to converse with you, Can I know your name?",
				"Hey, I am Carmina. May I know your name?"
			],
			"priority": "high",
			"type": {
				"type": "string"
			}
		}
	},
	{

		"schema": "SLOT_V1",
		"tags": ["Price"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Price",
			"slot_questions": [
				"What is the price limit you are looking for?"
			],
			"priority": "high",
			"type": {
				"type": "enum",
				"enum": [
					"upto 10000",
					"upto 30000"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["SIM"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "SIM",
			"slot_questions": [
				"How many sim slots are you looking for?"
			],
			"priority": "low",
			"type": {
				"type": "enum",
				"enum": [
					"single",
					"dual"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["EMI"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "EMI",
			"slot_questions": [
				"Are you looking for an EMI option?"
			],
			"priority": "low",
			"type": {
				"type": "enum",
				"enum": [
					"yes",
					"no"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["Replacement"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Replacement",
			"slot_questions": [
				"Are you looking for a phone with replacement policy incase of some major repair?"
			],
			"priority": "low",
			"type": {
				"type": "enum",
				"enum": [
					"yes",
					"no"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["Exchange Offer"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Exchange Offer",
			"slot_questions": [
				"Are you planning to exchange your current mobile for a new one?"
			],
			"priority": "low",
			"type": {
				"type": "enum",
				"enum": [
					"yes",
					"no"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["Battery"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Battery",
			"slot_questions": [
				"How much battery power are you looking for?"
			],
			"priority": "medium",
			"type": {
				"type": "enum",
				"enum": [
					"upto 3000 mAH",
					"above 3000 mAh"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["RAM"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "RAM",
			"slot_questions": [
				"How much RAM are you looking for?"
			],
			"priority": "medium",
			"type": {
				"type": "enum",
				"enum": [
					"2-4 GB",
					"above 4 GB"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["Memory"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Memory",
			"slot_questions": [
				"How much memory are you looking for?"
			],
			"priority": "medium",
			"type": {
				"type": "enum",
				"enum": [
					"64 GB",
                    "128 GB",
                    "256 GB"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["Display"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Display",
			"slot_questions": [
				"Whar are your smart phone display prefrences?"
			],
			"priority": "low",
			"type": {
				"type": "enum",
				"enum": [
					"IPS",
                    "TFT",
                    "Amoled"
				]
			}
		}
	},
	{
		"schema": "SLOT_V1",
		"tags": ["Camera"
		],
		"data": {
			"name": "Camera",
			"slot_questions": [
				"How much MP do you prefer?"
			],
			"priority": "medium",
			"type": {
				"type": "enum",
				"enum": [
					"2-5 MP",
					"5-8 MP"
				]
			}
		}
	}
]
}
            
            
            
input_json = {
  "context": {
    "name": "slot test integration",
    "example": "ex",
    "welcome": "welcome user",
    "welcome_returning": "welcome again",
    "next_question": "you can ask any question",
    "help_msg": "you can ask me more questions",
    "yes_no_help_msg": "yes no node",
    "bye": "bye. See you again. ",
    "error": "Sorry there seems to be some problem",
    "offer_long_answer_qa": "There seems to be a detailed",
    "offer_long_answer_es": "There is a detailed answer too",
    "no_answer": "Sorry we dont have an answer",
    "no_long": "There is no long answer",
    "no_long_but_email": "There is no long answer but email is available",
    "email_sent": "An email has been sent",
    "rest_api_base_url": "http://34.235.43.238:3128",
    "confidence_threshold": "0.5",
    "method_id": "sent2vec_v2",
    "template_id": "5bdbed93c9e77c00084670a0"
  },
  "data_set_list": [{
		"schema": "SLOT_V1",
		"tags": ["aName"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "aName",
			"slot_questions": [
				"Its nice to converse with you, Can I know your name?",
				"Hey, I am Carmina. May I know your name?"
			],
			"priority": "high",
			"type": {
				"type": "string"
			}
		}
	},
	{

		"schema": "SLOT_V1",
		"tags": ["Price"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "Price",
			"slot_questions": [
				"What is the price limit you are looking for?"
			],
			"priority": "high",
			"type": {
				"type": "enum",
				"enum": [
					"upto 10000",
					"upto 30000"
				]
			}
		}
    },
    {

		"schema": "SLOT_V1",
		"tags": ["SIM"
		],
		"data": {
			"slot_samples": [
				"some sample"
			],
			"name": "SIM",
			"slot_questions": [
				"How many sim slots are you looking for?"
			],
			"priority": "low",
			"type": {
				"type": "enum",
				"enum": [
					"single",
					"dual"
				]
			}
		}
    }
]
}
      

with open('payload_yaml.json','w') as f:
        json.dump(input_json, f)