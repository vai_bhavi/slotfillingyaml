#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 12:21:51 2018

@author: vaibhavi
"""

from flask import Flask, request, jsonify, Response
from flask_restplus import Api, Resource, fields, reqparse
from flask_cors import CORS
import logging as log
import sys
import json
import boto3
from FAQ_Parser3 import scrapeLink


#import logging
#from selenium.webdriver.remote.remote_connection import LOGGER
#LOGGER.setLevel(logging.WARNING)
#
#
#BUCKET_NAME = 'zappa-76bvozsno'
#DRIVER_NAME = './phantomjs'

#S3 = boto3.client('s3', region_name='us-east-1')

# Flask App
app = Flask(__name__)
api = Api(app, version='1.0', title='Scrape FAQ URL',
          description='')
cors = CORS(app, resources={r"/*": {"origins": "*"}})

#CORS(app, resources=r'/api/*')
#cors = CORS(app)
#CORS(app, expose_headers='Authorization')
#CORS(app, allow_headers='Content-Length')
#cors = CORS(app, resources={r"/*": {"origins": "*"}},headers="Content-Length")
#CORS(app, resources=r'/parse_url', allow_headers='Content-Type')

# Logging
root = log.getLogger()
root.setLevel(log.DEBUG)
ch = log.StreamHandler(sys.stdout)
ch.setLevel(log.DEBUG)
formatter = log.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

# config
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'

# expected arguments
swagger_arg = reqparse.RequestParser()
swagger_arg.add_argument('url', type=str, required=True, help='FAQ URL to parse')


@api.route('/parse_url')
@api.response(200, 'Parsing Successful')
@api.response(404, 'Error while Parsing URL.')
class SendEmail(Resource):

    @api.expect(swagger_arg)
    def post(self):

        log.info('Starting request %s', request)

        args = swagger_arg.parse_args(request)
        log.info(args)
        url = args.get('url', None)
        
        Bulk_QASet = scrapeLink(url)
        if len(Bulk_QASet)>0:
            print(len(Bulk_QASet))
        #response.headers.add('content-length', len(Bulk_QASet))
        response = jsonify({"response": Bulk_QASet})
        #response.headers.add('Access-Control-Allow-Origin', '*')
        #response.headers.add('content-type', 'application/json')
        return response

@api.route('/')
class HelloWorld(Resource):
    def get(self):
        return{"response":"success"}
        
@app.after_request
def add_cors_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Content-Length,Cache-Control,X-Requested-With,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Cache-Control,X-Requested-With,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
    return response

if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=80, debug=True)
    app.run()
