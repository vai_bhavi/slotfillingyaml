#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 14:29:23 2018

@author: vaibhavi
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 20:37:04 2018

@author: vaibhavi
"""

from bs4 import BeautifulSoup
import requests, json, re
import traceback

##### some example urls #####
#https://www.imdb.com/title/tt0068646/faq
#https://www.imdb.com/title/tt0468569/faq
#https://www.imdb.com/title/tt0848228/faq
#https://www.imdb.com/title/tt1375666/faq
# https://www.deldot.gov/About/faqs/index.shtml
######### ------- ##########



def scrapeLink(faqUrl):
    try:
        if faqUrl.split('//')[1].startswith('www.imdb.com'):
#            Bulk_QA_Set = scrapeLink2(faqUrl)
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            #values = soup.select("div[class*=faq]")         
            QA_List = []
            for num, (questions, answers) in enumerate(zip(soup.find_all("div", {"class": "faq-question-text"}),soup.find_all("div",{"class":"ipl-hideable-container ipl-hideable-container--hidden "}))):
                EachQA = dict()
                for question,answer in zip(questions,answers):
                    EachQA["question_set"] = [question]
                    EachQA["short_answer_set"] = [str(answers.text).lstrip().rstrip().replace('\n','')]
                    QA_List.append(EachQA)
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            #with open("BULKQASET.json","w") as f:
            #    json.dump(Bulk_QA_Set, f)   
            #return (json.dumps(Bulk_QA_Set))
            print("got json")
            return Bulk_QA_Set
        elif faqUrl.split('//')[1].startswith('www.samsung.com'):
            if faqUrl.split('//')[1] == 'www.samsung.com/us/support/customer-shopping-faqs/' or 'www.samsung.com/us/support/customer-shopping-faqs':
#                print(faqUrl)
                response = requests.get(faqUrl)
                soup = BeautifulSoup(response.text, "html.parser")
                value_Q = soup.find_all('a', {'name':re.compile(r"^questionID")})
                value_A = soup.select("div[class=Answer]") 
                QA_List = []
                for entryQ, entryA in zip(value_Q,value_A):
                    EachQA = dict()
                    question = entryQ.text
                    EachQA["question_set"] = [question.strip().lstrip().replace(u'\xa0', u'').replace('\n','').replace('\t','').replace('\r','')]
                    answer = entryA.text
                    EachQA["short_answer_set"] = [answer.strip().lstrip().replace(u'\xa0', u'').replace('\n','').replace('\t','').replace('\r','')]
                    QA_List.append(EachQA)
#                print(QA_List)
                Bulk_QA_Set = dict()
                Bulk_QA_Set["qa_set_list"] = QA_List
                return Bulk_QA_Set
        elif faqUrl.split('//')[1].startswith('www.deldot.gov'):
#            Bulk_QA_Set = scrapeLink1(faqUrl)
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            value_Q = soup.find_all('a', {"class": "show"})
            value_A = soup.select("div[class*=faq]") 
            QA_List = []
            for entryQ, entryA in zip(value_Q, value_A):
                EachQA = dict()
                if entryQ.text.startswith('Q.'):
                    question = entryQ.text.split('Q.')
                    EachQA["question_set"] = [question[1].lstrip().replace(u'\xa0', u'')]
                elif entryQ.text.startswith('Q:'):
                    question = entryQ.text.split('Q:')
                    EachQA["question_set"] = [question[1].lstrip().replace(u'\xa0', u'')]
                    
                if entryA.text.startswith('A:'):
                    answer = entryA.text.split('A:')
                    EachQA["short_answer_set"] = [answer[1].lstrip().replace(u'\xa0', u'')]
                QA_List.append(EachQA)
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            return Bulk_QA_Set
        elif faqUrl.split('//')[1].startswith('www.mcdonalds.com/us/en-us/mcdonalds-app/faqen.html'):
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            value_Q = soup.find_all('h3')
            value_A = soup.find_all('p') 
            QA_List = []
            for entryQ, entryA in zip(value_Q, value_A):
                EachQA = dict()
                if entryQ.text:
                    question = (entryQ.text)
                    EachQA["question_set"] = [question.lstrip().replace(u'\xa0', u'')]
                if entryA.text:
                    answer = entryA.text
                    EachQA["short_answer_set"] = [answer.lstrip().replace(u'\xa0', u'')]
#                    print(entryA.text)
                QA_List.append(EachQA)
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            return Bulk_QA_Set
        elif faqUrl.split('//')[1].startswith('www.mcdonalds.com/us/en-us/about-our-food/our-food-your-questions/burgers.html'):
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            value_Q = soup.find_all('a',{"class": "template-body-copy-heading"})
#            print(value_Q)
            value_A = soup.find_all('div',{"class":"template-body-copy collapse"})
            QA_List = []
            for entryQ, entryA in zip(value_Q, value_A):
                EachQA = dict()
                if entryQ.text:
                    question = (entryQ.text)
                    EachQA["question_set"] = [question.lstrip().strip().rstrip().replace(u'\xa0', u'').replace('\n','')]
                if entryA.text:
                    answer = entryA.text
                    EachQA["short_answer_set"] = [answer.lstrip().strip().rstrip().replace(u'\xa0', u'').replace('\n','')]
                QA_List.append(EachQA)
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            return Bulk_QA_Set    
        elif faqUrl.split('//')[1].startswith('www.mcdonalds.com/us/en-us/contact-us.html'):
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            value_Q = soup.find_all('a',{"class": "FAQSubTitle"})
            value_A = soup.find_all('div',{"class":"panel-collapse collapse q-a__answer"})
            QA_List = []
            for entryQ, entryA in zip(value_Q, value_A):
                EachQA = dict()
                if entryQ.text:
                    question = (entryQ.text)
                    EachQA["question_set"] = [question.lstrip().strip().rstrip().replace(u'\xa0', u'').replace('\n','')]
                if entryA.text:
                    answer = entryA.text
                    EachQA["short_answer_set"] = [answer.lstrip().strip().rstrip().replace(u'\xa0', u'').replace('\n','')]
                QA_List.append(EachQA)
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            return Bulk_QA_Set 
        elif faqUrl.split('//')[1].startswith('www.brigadegroup.com/faqs'):
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            value_faq = soup.find_all('div',{"class": 'accordion'})
            selector = 'div > h3 ~ div'
            value_Q = soup.find_all('h3')
            value_A = soup.select(selector)
            QA_List = []
            for entryQ, entryA in zip(value_Q,value_A):
                EachQA = dict()
                if entryQ.text.isspace() or entryQ.text == '' or entryA.text.isspace() or entryA.text == '':
                    print('empty')
                else:
                    question = entryQ.text
                    EachQA["question_set"] = [question.strip().lstrip().replace(u'\xa0', u'').replace('\n','').replace('\t','').replace('\r','')]
                    answer = entryA.text
                    EachQA["short_answer_set"] = [answer.strip().lstrip().replace(u'\xa0', u'').replace('\n','').replace('\t','').replace('\r','')]
                    QA_List.append(EachQA)
                        
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            return Bulk_QA_Set
        
        elif faqUrl.split('//')[1].startswith('www.mudra.org.in/FAQ'):
            response = requests.get(faqUrl)
            soup = BeautifulSoup(response.text, "html.parser")
            value_Q = soup.find_all('div',{"class":"panel-heading"}) 
            value_A = soup.find_all('div',{"class":re.compile(r'panel\-collapse collapse')})
            QA_List = []
            for entryQ, entryA in zip(value_Q, value_A):
#                EachQA = dict()
                for valQ,valA in zip(entryQ.find_all('en'),entryA.find_all('en')):
                    EachQA = dict()
                    question = re.sub("\s\s+" , " ", valQ.text) 
                    answer = re.sub("\s\s+" , " ", valA.text)
                    EachQA["question_set"] = [question.strip().lstrip().replace(u'\xa0', u'').replace('\n','').replace('\t','').replace('\r','')]
                    EachQA["short_answer_set"] = [answer.strip().lstrip().replace(u'\xa0', u'').replace('\n','').replace('\t','').replace('\r','')]
                    QA_List.append(EachQA)
            Bulk_QA_Set = dict()
            Bulk_QA_Set["qa_set_list"] = QA_List
            return Bulk_QA_Set
        
        else:
            Bulk_QA_Set = dict()
            return Bulk_QA_Set
        
    except:
        traceback.print_exc()
        Bulk_QA_Set = dict()
        #return (json.dumps(Bulk_QA_Set))
        return Bulk_QA_Set
#       https://www.mcdonalds.com/us/en-us/contact-us.html 
#QA = scrapeLink('https://www.mudra.org.in/FAQ')
#print(QA)