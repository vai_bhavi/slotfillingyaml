#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 13:06:02 2019
@author: vaibhavi

---Yaml Generation Script---
---Channels included: Alexa - alexa, Chat client - html5, Android - script
---APIs Integrated: NLP API, Krishna API
---All nodes and action flows are static
---The yaml generation follows just string reproduce logic
exit, next and other interrupts are to be added
"""


import json
import datetime

def generateYaml(context):
    
    yaml_content = ""
    
    ''' declaring yaml details '''
    #### yaml details
    yaml_content = yaml_content+'---\n'
    yaml_content = yaml_content+'#author: auto_generated\n'
    yaml_content = yaml_content+'#'+str(datetime.datetime.now())+"\n\n"
    
    nameProcessed = ""
    for eachname in context["name"].split():
        nameProcessed = nameProcessed+eachname+"_"

    #print(context["name"])

    yaml_content = yaml_content+'name: "'+nameProcessed[:-1]+'"\n'
    yaml_content = yaml_content+'name_pronounce: "'+context["name"]+'"\n'
    yaml_content = yaml_content+'version_int: 1\n'
    yaml_content = yaml_content+'yaml_version: "1.2"\n\n'
    
    yaml_content = yaml_content+'root_interceptor_ignore: ["ALEXA_STOP"]\n\n'
    
    
    ''' declaring yaml constants '''
    #### yaml constants
    
    yaml_content = yaml_content+'YAMLConstants:\n'
    yaml_content = yaml_content+'  name: "'+nameProcessed[:-1] +'"\n'
    yaml_content = yaml_content+'  name_pronounce: "'+context["name"]+'"\n'
    yaml_content = yaml_content+'  welcome_first_time: "'+context["welcome"]+'"\n'
    yaml_content = yaml_content+'  welcome_returning: "'+ context["welcome_returning"] +'"\n'
    yaml_content = yaml_content+'  next_question_prompt: "'+context["next_question"]+'"\n'
    yaml_content = yaml_content+'  help_message: "'+ context["help_msg"]+'"\n'
    yaml_content = yaml_content+'  no_answer_found: "'+context["no_answer"]+ '"\n'
    yaml_content = yaml_content+'  good_bye_message: "'+context["bye"]+'"\n'
    yaml_content = yaml_content+'  error: "'+context["error"]+'"\n'
    yaml_content = yaml_content+'  lead_prompt_reco: "'+context["lead_prompt_reco"]+'"\n'
    yaml_content = yaml_content+'  lead_prompt_reco_first_time: "'+context["welcome_recommendation"]+'"\n'
    yaml_content = yaml_content+'  initial_reco: "'+context["initial_reco"]+'"\n'
    
    try:
        if context["welcome_image_url"]:
            yaml_content = yaml_content+'  welcome_image_url: "'+context["welcome_image_url"]+'"\n\n'
    except:
        yaml_content = yaml_content+'  welcome_image_url: "''"\n\n'
    #yaml_content = yaml_content+'  restApiUri: "'+context["rest_api_base_url"] +'/api"\n'
    rest_api = "http://34.235.43.238:3128"
    yaml_content = yaml_content+'  nlpRestApiUri: "'+rest_api +'/api"\n'
    #nlpRestApiUri
    
#    yaml_content = yaml_content+'  example: "'+ context["example"] +'"\n'  
#    yaml_content = yaml_content+'  yesNoHelpMsg: "'+context["yes_no_help_msg"]+'"\n'
#    yaml_content = yaml_content+'  offerLongAnswerQA: "'+context["offer_long_answer_qa"]+'"\n'
#    yaml_content = yaml_content+'  offerLongAnswerES: "'+context["offer_long_answer_es"] +'"\n'
#    yaml_content = yaml_content+'  noLong: "'+context["no_long"] +'"\n'
#    yaml_content = yaml_content+'  noLongButEmail: "'+context["no_long_but_email"]+'"\n'
#    yaml_content = yaml_content+'  emailSent: "'+context["email_sent"]+'"\n'
#    yaml_content = yaml_content+'  welcomeImageUrl: "'+context["welcome_image_url"]+'"\n'
    
    
    ''' declaring cqaModelId, methodId, confidenceThreshold'''
    ### cqaModelId, methodId, confidenceThreshold    
    yaml_content = yaml_content+'  cqaModelId: "'+context["template_id"]+'"\n'
    yaml_content = yaml_content+'  methodId: "'+context["method_id"]+'"\n'
    yaml_content = yaml_content+'  confidenceThreshold: "'+context["confidence_threshold"]+'"\n'
    #yaml_content = yaml_content+'  krishnaRestApiUriDev: "'+context["krishnaRestApiUriDev"]+'"\n'
    #krishnaApi = "http://ec2-34-230-231-76.compute-1.amazonaws.com:9588"
    krishnaApi = "http://ec2-34-230-231-76.compute-1.amazonaws.com:9591"
    yaml_content = yaml_content+'  krishnaRestApiUriDev: "'+krishnaApi+'"\n'
    yaml_content = yaml_content+'  useKrishnaWelcomeBye: "'+context["use_krishna_welcome_bye"]+'"\n'
    yaml_content = yaml_content+'  useKrishnaReco: "'+context["use_krishna_reco"]+'"\n\n'

#    yaml_content = yaml_content+'help: "${dialog.CONST.help_message} <break time=\\"250ms\\"/> ${dialog.CONST.lead_prompt_reco} ${dialog.CONST.initial_reco}"\n\n'
    yaml_content = yaml_content+'help: "${dialog.CONST.help_message} <break time=\\"250ms\\"/> ${dialog.CONST.initial_reco}"\n\n'
    
    
    ''' NodeList begins here '''
    #### Nodelist declaration begins 
    yaml_content = yaml_content+'NodeList:\n'
    
    #''' welcomeNode '''
    #### welcomeNode actions
    # all actions are static for this node
#    yaml_content = yaml_content+'  welcomeNode:\n'
#    yaml_content = yaml_content+'    node_type: "WELCOMEPOINT"\n'
#    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
#    yaml_content = yaml_content+'    - action_01: "SAY"\n'
#    yaml_content = yaml_content+'      condition: "!avail(\'${global.current.returning_user}\')"\n'
#    yaml_content = yaml_content+'      reply: "${dialog.CONST.welcome_first_time}"\n'
#    
#    yaml_content = yaml_content+'    - action_02: "SAY"\n'
#    yaml_content = yaml_content+'      condition: "avail(\'${global.current.returning_user}\')"\n'
#    yaml_content = yaml_content+'      reply: "${dialog.CONST.welcome_returning}"\n'
#    
#    yaml_content = yaml_content+'    - action_03: "SET"\n'
#    yaml_content = yaml_content+'      set_variable: "${global.current.returning_user} TO true"\n'
#    
#    yaml_content = yaml_content+'    - action_04: "GOTO"\n'
#    yaml_content = yaml_content+'      get_user_response: true\n'
#    yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'
    
    ''' welcomeNode '''
    #### welcomeNode actions
    # all actions are static for this node
    yaml_content = yaml_content+'  welcomeNode:\n'
    yaml_content = yaml_content+'    node_type: "WELCOMEPOINT"\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "CALL_REST_API"\n'
    yaml_content = yaml_content+'      rest_01_name: "ENDPOINT_INFO"\n'
    yaml_content = yaml_content+'      rest_01_param: "{\'url\':\'${dialog.CONST.krishnaRestApiUriDev}\', \'method\': \'GET\', \'endpoint\': \'welcome_message\',\'param_name\': \'params\' }"\n'
    yaml_content = yaml_content+'      rest_02_name: "DATA_IN"\n'
    yaml_content = yaml_content+'      rest_02_param: "{\'user_id\': \'${client_info.user_id}\', \'cqa_model_id\': \'${dialog.CONST.cqaModelId}\', \'last_matched_q\':\'\', \'source\': \'${client_info.source}\'}"\n'
    yaml_content = yaml_content+'      rest_03_name: "RECOMMENDATIONS"\n'
    yaml_content = yaml_content+'      rest_03_param: "{\'route\': \'general_recommendation\', \'save_to\': \'reco_engine_welcome\' }"\n'

    yaml_content = yaml_content+'    - action_02: "SAY"\n'
    yaml_content = yaml_content+'      condition: "${dialog.CONST.useKrishnaWelcomeBye} == \'true\' && avail(\'${dialog.reco_engine_welcome.response}\') && ${dialog.reco_engine_welcome.response} != \'\' && ${dialog.reco_engine_welcome.response} != \'null\'"\n'
    yaml_content = yaml_content+'      reply: "${dialog.reco_engine_welcome.response}"\n'
    
    yaml_content = yaml_content+'    - action_03: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.temp_var.source} TO ${client_info.source}"\n'
    
    yaml_content = yaml_content+'    - action_04: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.temp_var.welcomeReco} TO true"\n'
    
    yaml_content = yaml_content+'    - action_05: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.botRepliesImage} TO ${dialog.CONST.welcome_image_url} IF ${client_info.source} == \'script\' && ${dialog.CONST.welcome_image_url} != \'\'"\n'
        
    yaml_content = yaml_content+'    - action_06: "GOTO"\n'
    yaml_content = yaml_content+'      get_user_response: false\n'
    yaml_content = yaml_content+'      goto_node: "getRecommendation IF avail(\'${dialog.reco_engine_welcome.response}\') && ${dialog.CONST.useKrishnaWelcomeBye} == \'true\'"\n'
    
    yaml_content = yaml_content+'    - action_07: "SAY"\n'
    yaml_content = yaml_content+'      condition: "!avail(\'${global.current.returning_user}\')"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.welcome_first_time}"\n'
    
    yaml_content = yaml_content+'    - action_08: "SAY"\n'
    yaml_content = yaml_content+'      condition: "avail(\'${global.current.returning_user}\')"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.welcome_returning} "\n'
    
    yaml_content = yaml_content+'    - action_09: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${global.current.returning_user} TO true"\n'
    
    yaml_content = yaml_content+'    - action_10: "GOTO"\n'
    yaml_content = yaml_content+'      get_user_response: false\n'
    yaml_content = yaml_content+'      goto_node: "getRecommendation IF ${dialog.CONST.useKrishnaReco} == \'true\'"\n'
    
    yaml_content = yaml_content+'    - action_11: "GOTO"\n'
    yaml_content = yaml_content+'      get_user_response: true\n'
    yaml_content = yaml_content+'      goto_node: "getAnswer"\n\n'
    
    ''' getAnswer '''
    #### getAnswer actions
#    # all actions are static for this node
    yaml_content = yaml_content+'  getAnswer:\n'
    yaml_content = yaml_content+'    node_type: "ENTRYPOINT"\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'

    yaml_content = yaml_content+'    - action_01: "UNSET"\n'
    yaml_content = yaml_content+'      unset_variable: "${dialog.botRepliesRcmd}"\n'
    
    yaml_content = yaml_content+'    - action_02: "UNSET"\n'
    yaml_content = yaml_content+'      unset_variable: "${dialog.botRepliesImage}"\n'
    
    yaml_content = yaml_content+'    - action_03: "UNSET"\n'
    yaml_content = yaml_content+'      unset_variable: "${dialog.botRepliesVideo}"\n'
    
    yaml_content = yaml_content+'    - action_04: "CALL_REST_API"\n'
    yaml_content = yaml_content+'      rest_01_name: "ENDPOINT_INFO"\n'
    yaml_content = yaml_content+'      rest_01_param: "{\'url\':\'${dialog.CONST.nlpRestApiUri}\', \'method\': \'GET\', \'endpoint\': \'/qa/\' }"\n'
    yaml_content = yaml_content+'      rest_02_name: "DATA_IN"\n'
    yaml_content = yaml_content+'      rest_02_param: "{\'text\': \'${dialog.userInputLast}\', \'source\': \'${client_info.source}\'}"\n'
    yaml_content = yaml_content+'      rest_03_name: "QA_NEW"\n'
    yaml_content = yaml_content+'      rest_03_param: "{ \'cqamodel_id\': \'${dialog.CONST.cqaModelId}\', \'save_to\': \'qa_response\' }"\n'
    
    yaml_content = yaml_content+'    - action_05: "SAY"\n'
    yaml_content = yaml_content+'      condition: "!avail(\'${dialog.qa_response}\') || !avail(\'${dialog.qa_response.api_call_status}\') || !${dialog.qa_response.api_call_status.ok}"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.error}"\n'
    
    yaml_content = yaml_content+'    - action_06: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "exitNode IF !avail(\'${dialog.qa_response}\') || !avail(\'${dialog.qa_response.api_call_status}\') || !${dialog.qa_response.api_call_status.ok}"\n'
    
    yaml_content = yaml_content+'    - action_07: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "byeNode IF (avail(\'${dialog.markRootIntent.flags.ALEXA_STOP}\') && ${dialog.markRootIntent.flags.ALEXA_STOP} == true)"\n'
    
    yaml_content = yaml_content+'    - action_08: "CARD_TEXT_FOR_NEXT_NODE"\n'
    yaml_content = yaml_content+'      cardTitle: "Alexa understood: "\n'
    yaml_content = yaml_content+'      cardText: "${dialog.userInputLast}"\n'

    yaml_content = yaml_content+'    - action_09: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.temp_var.source} TO ${client_info.source}"\n'

    yaml_content = yaml_content+'    - action_10: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "sayAnswer IF avail(\'${dialog.qa_response.api_qa.final_res}\') && (${dialog.qa_response.api_qa.final_res_conf} > ${dialog.CONST.confidenceThreshold})"\n'    
    
    yaml_content = yaml_content+'    - action_11: "SAY"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.no_answer_found} <break time=\\"250ms\\"/> (${dialog.CONST.next_question_prompt})"\n'
    
    yaml_content = yaml_content+'    - action_12: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "getRecommendation"\n\n'
    
    ''' sayAnswer '''
    #### sayAnswer actions
    # all actions are static for this node
    yaml_content = yaml_content+'  sayAnswer:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "SAY"\n'
    yaml_content = yaml_content+'      reply: "${dialog.qa_response.api_qa.final_res}"\n'
    
    yaml_content = yaml_content+'    - action_02: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.botRepliesVideo} TO ${dialog.qa_response.api_qa.final_play_video_link} IF (avail(\'${dialog.qa_response.api_qa.final_play_video_link}\') && ${dialog.qa_response.api_qa.final_play_video_link} != \'\' &&  ${dialog.qa_response.api_qa.final_play_video_link} != \'N/A\')"\n'
  
    yaml_content = yaml_content+'    - action_03: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.botRepliesImage} TO ${dialog.qa_response.api_qa.final_source_link} IF (avail(\'${dialog.qa_response.api_qa.final_source_link}\') && ${dialog.qa_response.api_qa.final_source_link} != \'\' && ${dialog.qa_response.api_qa.final_source_link} != \'N/A\')"\n'
 
    yaml_content = yaml_content+'    - action_04: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "getRecommendation"\n\n'
    
    ''' getRecommendation '''
    #### getRecommendation actions
    # all actions are static for this node
    yaml_content = yaml_content+'  getRecommendation:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "CALL_REST_API"\n'
    yaml_content = yaml_content+'      rest_01_name: "ENDPOINT_INFO"\n'
    yaml_content = yaml_content+'      rest_01_param: "{\'url\':\'${dialog.CONST.krishnaRestApiUriDev}\', \'method\': \'GET\', \'endpoint\': \'recommendation\',\'param_name\': \'params\' }"\n'
    yaml_content = yaml_content+'      rest_02_name: "DATA_IN"\n'
    yaml_content = yaml_content+'      rest_02_param: "{ \'user_id\': \'${client_info.user_id}\', \'cqa_model_id\': \'${dialog.CONST.cqaModelId}\', \'last_matched_q\': \'${dialog.qa_response.api_qa.final_hit_sample}\', \'source\': \'${client_info.source}\'}"\n'
    yaml_content = yaml_content+'      rest_03_name: "RECOMMENDATIONS"\n'
    yaml_content = yaml_content+'      rest_03_param: "{\'route\': \'general_reco\', \'save_to\': \'reco_engine_rcmd\' }"\n'
                    
    yaml_content = yaml_content+'    - action_02: "SAY"\n'
#    yaml_content = yaml_content+'      condition: "avail(\'${dialog.reco_engine_rcmd.response}\') && !avail(\'${dialog.temp_var.welcomeReco}\') && ${client_info.source} == \'alexa\'\"\n'
    yaml_content = yaml_content+'      condition: "avail(\'${dialog.reco_engine_rcmd.response}\') && ${client_info.source} == \'alexa\'\"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.lead_prompt_reco}"\n'
    
    yaml_content = yaml_content+'    - action_03: "SAY"\n'
#    yaml_content = yaml_content+'      condition: "avail(\'${dialog.reco_engine_rcmd.response}\') && !avail(\'${dialog.temp_var.welcomeReco}\') && ${client_info.source} == \'alexa\'\"\n'
    yaml_content = yaml_content+'      condition: "avail(\'${dialog.reco_engine_rcmd.response}\') && ${client_info.source} == \'alexa\'\"\n'
    yaml_content = yaml_content+'      reply: "${dialog.reco_engine_rcmd.response}"\n'
    
    yaml_content = yaml_content+'    - action_04: "SET"\n'
    yaml_content = yaml_content+'      set_variable: "${dialog.botRepliesRcmd} TO ${dialog.reco_engine_rcmd.response} IF avail(\'${dialog.reco_engine_rcmd.response}\') && ${client_info.source} == \'html5\' || ${client_info.source} == \'script\'"\n'
    
    yaml_content = yaml_content+'    - action_05: "UNSET"\n'
    yaml_content = yaml_content+'      unset_variable: "${dialog.temp_var.welcomeReco}"\n'
    
#    yaml_content = yaml_content+'    - action_06: "SAY"\n'  
#    yaml_content = yaml_content+'      condition: "!avail(\'${dialog.reco_engine_rcmd.response}\') && ${client_info.source} == \'alexa\'"\n'
#    yaml_content = yaml_content+'      reply: "${dialog.CONST.initial_reco}"\n' 
          
    yaml_content = yaml_content+'    - action_06:  "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "getAnswer"\n'
    yaml_content = yaml_content+'      get_user_response: true\n\n'
    
      
    ''' byeNode '''
    #### byeNode actions
    # all actions are static for this node
#    yaml_content = yaml_content+'  byeNode:\n'
#    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
#    yaml_content = yaml_content+'    - action_01: "SAY"\n'
#    yaml_content = yaml_content+'      reply: "${dialog.CONST.good_bye_message}"\n'
#    
#    yaml_content = yaml_content+'    - action_02: "GOTO"\n'
#    yaml_content = yaml_content+'      goto_node: "exitNode"\n\n'
    
#    ''' byeNode '''
#    #### byeNode actions
#    # all actions are static for this node
    yaml_content = yaml_content+'  byeNode:\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
#    yaml_content = yaml_content+'    - action_01: "CALL_REST_API"\n'
#    yaml_content = yaml_content+'      rest_01_name: "ENDPOINT_INFO"\n'
#    yaml_content = yaml_content+'      rest_01_param: "{\'url\':\'${dialog.CONST.krishnaRestApiUriDev}\', \'method\': \'GET\', \'endpoint\': \'leaving_message\',\'param_name\': \'params\' }"\n'
#    yaml_content = yaml_content+'      rest_02_name: "DATA_IN"\n'
#    yaml_content = yaml_content+'      rest_02_param: "{ \'user_id\': \'${client_info.user_id}\', \'cqa_model_id\': \'${dialog.CONST.cqaModelId}\', \'last_matched_q\': \'\', \'source\': \'${client_info.source}\' }"\n'
#    yaml_content = yaml_content+'      rest_03_name: "RECOMMENDATIONS"\n'
#    yaml_content = yaml_content+'      rest_03_param: "{\'route\': \'general_reco\', \'save_to\': \'reco_engine_bye\' }"\n'
#    
#    yaml_content = yaml_content+'    - action_02: "SAY"\n'
#    yaml_content = yaml_content+'      condition: "avail(\'${dialog.reco_engine_bye.response}\')"\n'
#    yaml_content = yaml_content+'      reply: "${dialog.reco_engine_bye.response}"\n'
#    
#    yaml_content = yaml_content+'    - action_03: "GOTO"\n'
#    yaml_content = yaml_content+'      goto_node: "exitNode IF avail(\'${dialog.reco_engine_bye.response}\')"\n'

    yaml_content = yaml_content+'    - action_01: "SAY"\n'
    yaml_content = yaml_content+'      reply: "${dialog.CONST.good_bye_message}"\n'
    
    yaml_content = yaml_content+'    - action_02: "GOTO"\n'
    yaml_content = yaml_content+'      goto_node: "exitNode"\n\n'
    
    ''' exitNode '''
    #### exitNode actions
    # all actions are static for this node
    yaml_content = yaml_content+'  exitNode:\n'
    yaml_content = yaml_content+'    node_type: "EXITPOINT"\n'
    yaml_content = yaml_content+'    list_of_actions_to_perform:\n'
    yaml_content = yaml_content+'    - action_01: "END"\n'
    
    #print(yaml_content)
    return yaml_content
    
def main_yamlGeneration(json_filename):
        try:
            yaml_content = generateYaml(json_filename["context"])
            print("@@@@@@Android Yaml Generation Complete@@@@@@@")
        except:
            yaml_content = ""
        return yaml_content
#        
#with open("payload_qa2.json") as f:
#    data = json.load(f)
#yaml_content = main_yamlGeneration_generic(data)
#with open('new_generic_template.yaml','w') as f:
#    f.write(yaml_content)   


