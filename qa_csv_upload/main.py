from flask import Flask, make_response, request, jsonify, Response
from flask_restplus import Api, Resource, fields, reqparse
from flask_cors import CORS
import io
import csv
import logging as log
import sys
import json
import os
from upload_application1 import processCsvString

app = Flask(__name__)
api = Api(app, version='1.0', title='Upload QA CSV',
          description='')
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# Logging
root = log.getLogger()
root.setLevel(log.DEBUG)
ch = log.StreamHandler(sys.stdout)
ch.setLevel(log.DEBUG)
formatter = log.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

# @app.route('/')
# def form():
#     return """
#         <html>
#             <body>
#                 <h1>Upload a csv file</h1>

#                 <form action="/qa_csv" method="post" enctype="multipart/form-data">
#                     <input type="file" name="data_file" />
#                     <input type="submit" />
#                 </form>
#             </body>
#         </html>
#     """

@app.route('/upload_qa_csv', methods=["POST"])
#@app.response(200, 'Csv Upload Successful')
#@app.response(404, 'Error while qa csv upload')
def transform_view():
    log.info('Starting request %s', request)
    f = request.files['data_file']
    #f = request.files.items()
    if not f:
        return {}
    stream = io.StringIO(f.stream.read().decode("UTF8"), newline=None)
    stream.seek(0)
    data = stream.read()
    csv_content = []
    for row in csv.DictReader(data.splitlines()):
        csv_content.append(row)
    print(csv_content)
    log.info(csv_content)
    Bulk_QASet = processCsvString(csv_content)
    response = jsonify({"response": Bulk_QASet})
    print(response)
    return response

@app.after_request
def add_cors_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Content-Length,Cache-Control,X-Requested-With,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Cache-Control,X-Requested-With,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
    return response

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)