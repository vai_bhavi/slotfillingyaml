#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 13:51:33 2019

@author: vaibhavi
"""

import json
import re
import csv

def remove_non_ascii(text):
    """
    Remove non-ascii chars from text.
    :param text: input text
    :return: formatted text
    """

    try:
        text.encode('ascii')
    except UnicodeEncodeError:
        print('Removing non-ascii chars from:', text)
        text = ''.join(i for i in text if ord(i) < 128)

    try:
        text.encode('ascii')
    except UnicodeEncodeError:
        print('Error !!! Still non-ascii chars remain !!!')

    return text

def dollarProcessing(text):
    newText = ""
    if '$' in text.split():
        index = 0
        for token in text.split():
            if index < len(text.split()):
                if token == '$':
                    token = text.split()[index+1]+ " "+"dollars "
                    newText = newText+ token
                    index = index+2
                else:
                    newText = newText + text.split()[index] +" "
                    index = index + 1
        text = newText
    elif '$' in text:
        for token in text.split():
            if '$' in token[0] and token[1] != '$':
                token = token[1:]+" dollars"
            newText = newText+token+" "
        text = newText
    return text

def format_text(text, add_ending=False, asciify=False):
    """
    Format the input text.
    :param text: input text
    :param add_ending: add '. ' at the end if needed
    :param asciify: try to remove non-ascii chars
    :return: formatted text
    """

    text = text.strip()
    # text = text.replace('b\'', '')
    # text = text.replace('xe2', '')
    # text = text.replace('\\xe2', '')
    # text = text.replace('\\x80', '')
    # text = text.replace('\\\\x99', '')
    # text = text.replace('\\\\', '')
    text = dollarProcessing(text)
    text = text.replace('\ufeff', '')
    text = text.replace('\u2019', '\'')
    text = text.replace('’', '\'')
    text = text.replace('“', '"')
    text = text.replace('”', '"')
    text = text.replace('–', '-')

    if add_ending:
        if not text[-1] in ['.', '?', '!']:
            text += '.'
        text += ' '

    if asciify:
        text = remove_non_ascii(text)

    return text

def BasicFieldProcessing(csv_content):
    json_content = []
    flag_invalidQA = 0
    
    for entry in csv_content:
        json_entry = {}
        ### *entry helps in getting the keys from dictionary
        ## so we use the actual key and this avoids the mismatch due to cases
        ## first entry should be query - expected from user
        if entry[[*entry][0]].lower():
            query = {}
            query["text"] = format_text(entry[[*entry][0]])
            json_entry['query'] = [query]
        else:
            query["text"] = ""
            flag_invalidQA = 1
            break
        ## second entry should be response - expected from user
        if entry[[*entry][1]].lower():
            resp = {}
            resp["text"] = format_text(entry[[*entry][1]])
            json_entry['response'] = [resp]
        else:
            resp["text"] = ""
            flag_invalidQA = 1
            break
        ## validation check -- query and response should be non-empty
        if bool(query) and bool(resp):
#            print("valid")
            if not entry[[*entry][0]].lower().isspace() and not entry[[*entry][1]].lower().isspace():
                if (query["text"] != "")  and (resp["text"] != ""):
                    json_content.append(json_entry)
#                    print(resp["text"])
#                    print("------>>>")
#                    print(json_content)
#    print(json_content)
    if flag_invalidQA == 1:
        return []
    else:
        return json_content
    
def processCsvString(csv_string):
    ''' *case 1 - create application flow csv processing 
        only 2 coulmns is expected - query and response '''
    
    # with open('output_two.csv','rb') as csvfile:
    #     reader = csv.DictReader(csvfile,delimiter=",")
    #     csv_content = (list(reader))
    # print(csv_content)
        
    try:
        json_content = BasicFieldProcessing(csv_string)
#        print(json_content)
    except:
        json_content = []
    return json_content 
#    return json.dumps(json_content)

#with open('output_check.csv','r') as csvfile:
#    reader = csv.DictReader(csvfile,delimiter=",")
#    csv_content = (list(reader))
#
#json_content = processCsvString(csv_content)
#print(json_content)





