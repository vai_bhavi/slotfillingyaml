#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 20:21:46 2018

@author: vaibhavi
dollar processing added
"""
"""
Notebook for converting the Google files to json files used by Falcon.

@tomas.brich

TODO: In which fields can we keep non-ascii chars? Voice vs FB...
"""

import re
import csv
import json
import copy
import yaml
from enum import Enum, unique

#from concertonlp.nlp.common import expand_sentence
#from concertonlp.nlp.template_manager import FalconTemplateEnum

RE_PIPE = re.compile(r"\(.*?\|.*?\)")


@unique
class FalconTemplateEnum(Enum):
    documents = 'qa_set_list'
    samples = 'question_set'
    short = 'short_answer_set'
    long = 'long_answer_set'
    email = 'email_answer_set'
    facebook = 'fb_answer_set'
    source_say = "source_say_set"
    source_link = "source_link_set"
    rcmd_short = "short_rcmd_set"
    rcmd_long = "long_rcmd_set"


fte = FalconTemplateEnum


def expand_sentence(sent: (str, list)) -> list:
    """
    The input sentence in the form: I (like|dislike) (you|her) is expanded into several sentences.
    :param sent: input sentence
    :return: expanded sentence
    """
    if isinstance(sent, list):
        ll = []
        for s in sent:
            ll.extend(expand_sentence(s))
        return ll

    # this placeholder is used in the ''.format(placeholder)
    s_placeholder = '{placeholder}'
    re_res = RE_PIPE.search(sent)
    results = []

    if re_res:
        # string to be replaced
        m = re_res.group()

        # do not allow nested pipes, e.g. (aaa|(aaa|bbb)) ccc
        if m:
            assert '(' not in m[1::]
            assert ')' not in m[:-1]
        expand_by_str = m.replace('(', '').replace(')', '').split('|')

        # replace only the first occurrence
        sent_with_placeholder = re.sub(RE_PIPE, s_placeholder, sent, count=1)

        for s in expand_by_str:
            s = s.strip()
            new_sent = sent_with_placeholder.format(placeholder=s)
            if not s:
                # normalize white space
                new_sent = new_sent.replace("  ", " ")

            re_res = RE_PIPE.search(new_sent)
            if re_res:
                results.extend(expand_sentence(new_sent))
            else:
                results.extend([new_sent])

        return results

    return [sent]



def remove_non_ascii(text):
    """
    Remove non-ascii chars from text.
    :param text: input text
    :return: formatted text
    """

    try:
        text.encode('ascii')
    except UnicodeEncodeError:
        print('Removing non-ascii chars from:', text)
        text = ''.join(i for i in text if ord(i) < 128)

    try:
        text.encode('ascii')
    except UnicodeEncodeError:
        print('Error !!! Still non-ascii chars remain !!!')

    return text

def dollarProcessing(text):
    newText = ""
    if '$' in text.split():
        index = 0
        for token in text.split():
            if index < len(text.split()):
                if token == '$':
                    token = text.split()[index+1]+ " "+"dollars "
                    newText = newText+ token
                    index = index+2
                else:
                    newText = newText + text.split()[index] +" "
                    index = index + 1
        text = newText
    elif '$' in text:
        for token in text.split():
            if '$' in token[0]:
                token = token[1:]+" dollars"
            newText = newText+token+" "
        text = newText
    return text

def format_text(text, add_ending=False, asciify=False):
    """
    Format the input text.
    :param text: input text
    :param add_ending: add '. ' at the end if needed
    :param asciify: try to remove non-ascii chars
    :return: formatted text
    """

    text = text.strip()
    # text = text.replace('b\'', '')
    # text = text.replace('xe2', '')
    # text = text.replace('\\xe2', '')
    # text = text.replace('\\x80', '')
    # text = text.replace('\\\\x99', '')
    # text = text.replace('\\\\', '')
    text = dollarProcessing(text)
    text = text.replace('\ufeff', '')
    text = text.replace('\u2019', '\'')
    text = text.replace('’', '\'')
    text = text.replace('“', '"')
    text = text.replace('”', '"')
    text = text.replace('–', '-')

    if add_ending:
        if not text[-1] in ['.', '?', '!']:
            text += '.'
        text += ' '

    if asciify:
        text = remove_non_ascii(text)

    return text


def qa_txt_to_json(data):
    """
    Convert QA data in Google Doc .txt format to json.
    :param data: data read from the .txt file
    :return: json data dumps
    """

    fields_default = {
        'Q': [],
        'A_SHORT': [],
        'A_LONG': [],
        'A_FACEBOOK': [],
        'A_EMAIL': [],
        'RCMD_SHORT': ['( Start Quiz | Live Agent )'],
        'RCMD_LONG': ['( Start Quiz | Live Agent )'],
        'SOURCE_LINK': [],
        'VIDEO_LINK': []
    }
    fields = copy.deepcopy(fields_default)

    counter = {
        'Q_EXPANDED': 0,
        'Q': 0,
        'A_SHORT': 0,
        'A_LONG': 0,
        'A_FACEBOOK': 0,
        'A_EMAIL': 0,
        'RCMD_SHORT': 0,
        'RCMD_LONG': 0,
        'SOURCE_LINK': 0,
        'VIDEO_LINK': 0
    }

    data = re.sub('[\[].*?[\]]', '', data)  # remove google doc comments
    data = data.splitlines()

    documents = []
    filled = False

    for line in data:
        # skip irrelevant lines
        line = line.split(':', 1)
        if line[0] not in fields:
            if line[0]:
                print('Ignoring line:', line)
            continue

        tag, line = line[0], format_text(line[1], asciify=True)

        # add Start Quiz and Live Agent to recommendations
        if tag.startswith('RCMD'):
            line = line.rstrip(')')
            line += '| Start Quiz | Live Agent )'
            fields[tag] = []

        if tag.startswith('A_'):
            filled = True

        if tag == 'Q' and filled:
            documents.append(fields)
            filled = False
            fields = copy.deepcopy(fields_default)

        fields[tag].append(line)
        counter[tag] += 1

    falcon_output = []
    for doc in documents:
        expanded = expand_sentence(doc['Q'])
        counter['Q_EXPANDED'] += len(expanded)
        falcon_output.append({
            fte.samples.value: expanded,
            fte.short.value: doc['A_SHORT'],
            fte.long.value: doc['A_LONG'] if doc['A_LONG'] else None,
            fte.facebook.value: doc['A_FACEBOOK'] if doc['A_FACEBOOK'] else None,
            fte.email.value: doc['A_EMAIL'] if doc['A_EMAIL'] else None,
            fte.rcmd_short.value: doc['RCMD_SHORT'] if doc['RCMD_SHORT'] else None,
            fte.rcmd_long.value: doc['RCMD_LONG'] if doc['RCMD_LONG'] else None,
            fte.source_link.value: doc['SOURCE_LINK'] if doc['SOURCE_LINK'] else None,
            fte.source_say.value: doc['VIDEO_LINK'] if doc['VIDEO_LINK'] else None
        })

    print('\nStatistics:')
    for key, value in counter.items():
        print('\t', key, value)

    falcon_output = {fte.documents.value: falcon_output}
    return json.dumps(falcon_output, indent=2)


def qa_csv_to_json(data):
    """
    Convert QA data in Google Spreadsheet .csv format to json.
    :param data: loaded csv data
    :return: json data dumps
    """

    data = data.splitlines()
    reader = csv.reader(data)
    next(reader)
    csv_data = [line for line in reader]

    output = []
    for line in csv_data:
        questions = [line[3]] + [q for q in line[9:] if q]
        expanded = []
        for q in questions:
            expanded += expand_sentence(q)
        expanded = [format_text(q, asciify=True) for q in expanded]

        tags = line[0].split(', ') if line[0] else None
        a_short = [format_text(line[4], asciify=True)] if line[4] else None
        a_long = [format_text(line[5], asciify=True)] if line[5] else None
        a_fb = [format_text(line[6], asciify=True)] if line[6] else None
        rcmd_short = [line[7]] if line[7] else None
        rcmd_long = [line[8]] if line[8] else None

        output.append({
            'tags': tags,
            fte.samples.value: expanded,
            fte.short.value: a_short,
            fte.long.value: a_long,
            fte.facebook.value: a_fb,
            fte.rcmd_short.value: rcmd_short,
            fte.rcmd_long.value: rcmd_long
        })

    falcon_output = {fte.documents.value: output}
    return json.dumps(falcon_output, indent=2)


def qa_json_to_json(data):
    """
    Format entries in given QA json data.
    :param data: loaded json data
    :return: json data dumps
    """

    data = json.loads(data)

    for document in data[fte.documents.value]:
        for key, value in document.items():
            if value:
                document[key] = [format_text(el, asciify=True) for el in value]

    return json.dumps(data, indent=2)


def quiz_csv_to_json(data, quiz_name):
    """
    Convert Quiz data in Google Spreadsheet .csv format to json.
    :param data: loaded csv data
    :param quiz_name: name of the quiz
    :return: json data dumps
    """

    data = data.splitlines()
    reader = csv.reader(data)
    next(reader)
    csv_data = [line for line in reader]

    fb_num = {1: '\\u24f5', 2: '\\u24f6', 3: '\\u24f7', 4: '\\u24f8'}

    falcon_output = {
        'schema': 'QUIZ_V1',
        'tags': None,
        'data': {'quiz_name': quiz_name, 'quiz_qa': [], 'quiz_qa_fb': []}
    }
    quiz_qa = falcon_output['data']['quiz_qa']
    quiz_qa_fb = falcon_output['data']['quiz_qa_fb']

    for line in csv_data:
        question = format_text(line[1])
        correct = int(line[2])

        qa_dict = {'question': f' {question} ', 'answer': []}
        qa_fb_dict = {'question': f'*{question}*', 'answer': []}

        for i, q in enumerate(line[3:], 1):
            if not q:
                break

            q = format_text(q, add_ending=True)
            qa_dict['answer'].append({'text': f'{i}, {q}', 'correct': i == correct})
            qa_fb_dict['answer'].append({'text': f'\\u0009 {fb_num[i]} {q}', 'correct': i == correct})

        # qa_fb_dict['answer'][-1]['text'] += '\\r'

        quiz_qa.append(qa_dict)
        quiz_qa_fb.append(qa_fb_dict)

    print('\nQuestions loaded:', len(quiz_qa))

    return json.dumps(falcon_output, indent=2)


def quiz_json_to_json(data):
    """
    Format entries in given Quiz json data.
    :param data: loaded json data
    :return: json data dumps
    """

    fb_num = {1: '\\u24f5', 2: '\\u24f6', 3: '\\u24f7', 4: '\\u24f8'}
    data = json.loads(data)

    # format data for voice
    for document in data['data']['quiz_qa']:
        document['question'] = f" {format_text(document['question'])} "

        for i, entry in enumerate(document['answer'], 1):
            text = format_text(entry['text'], add_ending=True)
            if not text.startswith(str(i)):
                text = f'{i}, {text}'
            entry['text'] = text

    # format data for facebook (rewrite them / do nothing if they are not there)
    if 'quiz_qa_fb' in data['data']:
        data['data']['quiz_qa_fb'] = copy.deepcopy(data['data']['quiz_qa'])
        for document in data['data']['quiz_qa_fb']:
            document['question'] = f"*{document['question'].strip()}*"

            for i, entry in enumerate(document['answer'], 1):
                entry['text'] = entry['text'].replace(f'{i},', f'\\u0009 {fb_num[i]}')

    return json.dumps(data, indent=2)


#if __name__ == '__main__':
def Processor(file,filetype):   
    
#    PATH = '../data/ETS_QA_3.0.txt'
#    PATH_OUT = '../data/ETS_QA_3.0.json'
    
    # PATH = '../data/ETS_QA.csv'
    # PATH_OUT = '../data/ETS_QA.json'
    
    # PATH = '../data/ETS_QA_3.0.json'
    # PATH_OUT = '../data/ETS_QA_3.0_formatted.json'

    # PATH = '../data/ETS_Quiz.csv'
    # PATH_OUT = '../data/ETS_Quiz.json'

    # PATH = '../data/ETS_Quiz.json'
#    PATH_OUT = 'ETS_formatted.json'
#    file = 'ETS_QA_Sample.txt'
    
    with open(file) as f:
        txt = f.read()
        
    if filetype == "txt":
        json_out = qa_txt_to_json(txt)
    elif filetype == "csv":
        json_out = qa_csv_to_json(txt)
    elif filetype == "json":
        json_out = qa_json_to_json(txt)
        
    ''' quiz qa processing '''    
    # json_out = quiz_csv_to_json(txt, quiz_name='ETS Quiz test')
    # json_out = quiz_json_to_json(txt)
    print(json_out)
    return json_out
#    with open(PATH_OUT, 'w+') as f:
#        f.write(json_out)

#file = 'ETS_QA_Sample.txt'
#filetype = "txt"
file = 'ETS_QA_Sample.csv'
filetype = "csv"
Processor(file,filetype)
