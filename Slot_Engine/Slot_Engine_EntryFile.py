#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 13:07:40 2019

@author: vaibhavi
"""

''' Slot Engine '''

############## 

import re


## AskSlotQuestionAndOptions
def ReturnSlotQO(slot_details, source):
    if slot_details['question'].strip() != "":
        if source == 'alexa': ## voice channels
            ## do processing
            if slot_details['type'] == 'enum' and slot_details['options'] != "":
                options = "choose one of the following "
                for i,each_option in enumerate(slot_details['options'], start = 1):
                    options = options+"option "+str(i)+" "+each_option+" "
            print(options)
            return slot_details['question'], options
        else: 
            return slot_details['question'], slot_details['options']
        
            ##
    ### This function can be used to process the slot question and options
    ### if we require some slot-type specific processing
    

##############################################################################

def ValidateSlotValue(cqa_model_id, session_id, source, slot_id, response):
    Validate_NumericalValue(response)
    Validate_StringValue(response)
    Validate_AgeValue(response)
    Validate_emailValue(response)
#    Validate_teleNumber(response)
    Validate_ListValue(response, slot_id)
#    Validate_customValue(response, slot_id)
    ### the response from user could be a new query and not a response
#    Validate_ifQuery(response)
    return ""

def Validate_NumericalValue(response):
    ## allows only +ve numbers
    if response.isdigit():
        return True
    else:
        return False
    
def Validate_StringValue(response):
    ## allows alphabets and white spaces
    if re.match('^[a-zA-Z\s]*$', response):
        return True
    else:
        return False
    
def Validate_AgeValue(response):
    ## allows only +ve values from 1 to 120
    if response.isdigit() and int(response) > 0 and int(response) <= 120:
        return True
    else:
        return False
        
def Validate_emailValue(response):
    # pip install validate_email
    from validate_email import validate_email
    if validate_email(response):
        return True
    else:
        return False
    
#def Validate_teleNumber(response):
#    import phonenumbers
#    from phonenumbers import carrier
#    from phonenumbers.phonenumberutil import number_type
        
##    number = "+49 176 1234 5678"
##    number = "+442083661177"
##    stat = carrier._is_mobile(number_type(phonenumbers.parse(number,None)))
#    stat = phonenumbers.parse(number, None)
##    print(stat)
#    return stat

##############################################################################
    
def checkSlotStatus(slot_details):
    print('---checking slot details---')
    if slot_details['value'].strip() == '':
        print('unfilled')
        return False
    else:
        print('filled')
        return True
    
##############################################################################
    
def increment_turnCounter(session_id):
    ### move into the turn counter value of the session id
    ### increment the session_id
    print("incremented turn counter by 1")


##############          Case 1 - Endpoint1           ############## 
## If tag found in the QA in yaml, control will be passed here   ##
def tagBasedTrigger(cqa_model_id, session_id, source, QA_id):
    print("-----------invoking tag based trigger-------------")
    ### retrieving slots associated with the particular QA asked by user
    slot_list = retrieveQA_SlotData(cqa_model_id, session_id, source, QA_id)
   
    ## if only one tag associated with the QA_id
    if len(slot_list) == 1:
        print("---only one tag---")
        ## check if slot is filled or not
        slot_status = checkSlotStatus(slot_list[0])
        ## if slot is unfilled
        if slot_status == False:
            print("process slot QO")
            ## process the slot question and options according to channel
            return ReturnSlotQO(slot_list[0], source)
            
        ## if slot is already filled
        else:
            ## check turn counter's status
            ## if turn counter has reached the threshold
            turn_status = turnCounter_Status(session_id)
            ## if turnCounter has reached threshold,then invoke turnBasedTrigger
            if turn_status == True:
                print("process slot QO")
                turnBasedTrigger(cqa_model_id, session_id, source, QA_id)
            ## otherwise increment counter value by one and move on
            else:
                increment_turnCounter(session_id)
                
    ## if more than one tag associated with the QA_id
    else:
        print("---more than one tag---")
        ## flag to help understand unfilled slot's presence/absence 
        slot_processing = 0
        ## check for the first unfilled slot from the list of associated slots
        for each_slot in slot_list:
            slot_status = checkSlotStatus(each_slot)
            ## if even one unfilled slot associated, then process it
            if slot_status == False:
                print("process slot QO")
                ## set flag to 1 if unfilled slot encountered
                slot_processing = 1
                ## process the slot question and options according to channel
                return ReturnSlotQO(slot_list[0], source)
                
        ## if flag is 0, means all slots associated with the QA_id are filled
        ## check for turnCounter status to invoke turnBasedCounter
        if slot_processing == 0:
            print("all slots/tags associated with the QA_id is filled")
            turn_status = turnCounter_Status(session_id)
            ## if turnCounter has reached threshold,then invoke turnBasedTrigger
            if turn_status == True:
                print("process slot QO")
                turnBasedTrigger(cqa_model_id, session_id, source, QA_id)
            ## otherwise increment counter value by one and move on
            else:
                increment_turnCounter(session_id)
    ## return empty string for slot_question and slot_options if none of the
    ##          valid options are met
    return "",""

##############################################################################

##############          Case 2 - Endpoint2         ##################
## If turn_counter has reached the fixed_turn_value limit then 
#                                  this case will be activated   ##
def turnBasedTrigger(cqa_model_id, session_id, source, QA_id):
    # turn_trigger will be true to come to this fn
    print("invoking turn based trigger, rcvd the cqa_id")
    ## return the slot_list for that cqa id
    slot_list = retrieveCQA_SlotData(cqa_model_id, session_id)
    ## selects unfilled slot according order of priority
    selected_slot = select_slot(slot_list)
    ## if empty dict is returned,all slots filled - user's current session
    if len(selected_slot.keys()) == 0:
        print("no slots to fill")
    ## otherwise a slot to be queried by bot will be returned
    else:
        ## process the slot question and options according to channel
        return ReturnSlotQO(selected_slot, source)
    return "",""

## slot_list of the cqa_id is passed to this function
## slot_list corresponds to current user's slot_info for that particular cqa_id
def select_slot(slot_list):
    selected_flag = 0
    print(slot_list)
    for each_slot in slot_list:
        if each_slot["priority"] == "high" and each_slot["value"].strip() == "" :
            selected_flag = 1
            return each_slot
    for each_slot in slot_list:
         if each_slot["priority"] == "mid" and each_slot["value"].strip() == "" :
             selected_flag = 1
             return each_slot
    for each_slot in slot_list:
         if each_slot["priority"] == "low" and each_slot["value"].strip() == "" :
            selected_flag = 1
            return each_slot
    if selected_flag == 0:
        return {}
    
##############################################################################

### will be invoked in the case of the tag based trigger
### obtain all tags/slots for that question using qa_id and cqa_model_id
## this call will be made to the DB holding the QA_set
def retrieveQA_SlotData(cqa_model_id, session_id, source, QA_id):
    ## hit the QA_set data, obtain the tags associated with the QA_id
    print('---obtaining tags of the QA_id---')
    ### assuming the tags of the QA
    ### from the QA_set we need to process and make a list with the foll properties:
    ### return a list of dictionaries from the QA_set for that QA_id's slot_list
    ### each dictionary should have:
    ### id, type, question, options, value, priority
    slot_list = [{'id':'401','name':'fav_color','type':'enum',
                  'question':'What is your fav color?',
                  'options':['red','blue','black'],'value':'',
                  'priority':'high'}, {'id':'402','name':'budget','type':'enum',
                 'question':'What is your preferred budget?',
                'options':['10000 to 20000', 'below 10000', 'above 20000'],
                'priority':'low','value':' y' }]
    ## type will help us in validating
    ## question for botQ
    ## options can be either '' or can have values separated by '|'
    ## value can be either '' or filled
    ## QA_id can be associated with only one tag/slot or with more than one
    ## hence we return a list of dictionaries, each dict - one slot/tag
    return slot_list
    #return tagList, slot_id_List
##############################################################################

### will be invoked in the case of the turn based trigger
### obtain all tags/slots for that project using cqa_model_id
## this call also will be made to the DB holding the QA_set
def retrieveCQA_SlotData(cqa_model_id, session_id):
    ## return for the whole project
    ## slot information is stored/retrieved as a dictionary
    ### return a list of dictionaries for that CQA_id 
    ### each dictionary should have:
    ### id, type, question, options, value, priority
    slot_list = [{'id':'401','name':'fav_color','type':'enum',
                  'question':'What is your fav color?',
                  'options':['red','blue','black'],'value':'', 
                  'priority':'high' }, 
                {'id':'402','name':'budget','type':'enum',
                 'question':'What is your preferred budget?',
                'options':['10000 to 20000', 'below 10000', 'above 20000'],
                'priority':'low','value':' y' }]
    return slot_list
#    return tagList, slot_id_List

##############################################################################
## should be obtained from the user's session info
## parameter should be user session
def turnCounter_Status(session_id):
    ### obtain turnConter value from user's session info
    ### obtain turnthreshold from user's session info or from falcon
    ### threshold should be set in the application
    turnThreshold = 3
    turnCounter = 3
    if turnCounter == turnThreshold:
        return True
    else:
        return False

##############################################################################
    
### entry function to the slotEngine
### call from yaml comes into this place
### we need to check if the call is appropriate to invoke a tag/turn trigger
###             and appropriately make the call
### the value of turn_trigger should be maintained for each user's session
''' definition of entry function'''
def slotQO_Engine(cqa_model_id, session_id, source, QA_id):
    ### we need to get the turn_counter from the user's session
    turnStatus = turnCounter_Status(session_id) 
    ## will be returned as True - if counter reached threshold
    ### we check on if turnStatus is set to True or False
    if turnStatus == False:
        slot_question, slot_options = tagBasedTrigger(cqa_model_id, session_id, source, QA_id)
    else:
        slot_question, slot_options = turnBasedTrigger(cqa_model_id, session_id, source, QA_id)
    return slot_question, slot_options
##############################################################################

    
''' entry point '''
### API call 
slot_question, slot_options = slotQO_Engine('5123456789','ses1234','html5','001')
#slot_question, slot_options = slotQO_Engine(cqa_model_id, session_id, source, QA_id)

######









    
    
    
    